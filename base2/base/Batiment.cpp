#include "Batiment.hpp"
#include <string.h>
#define GRANDETAILLE 300
#define PETITETAILLE 50
#define VIEMUR 1000
#define VIEMINE 100
#define VIEFORTIF 250
#define ORMINE 20

using namespace std;

MenuAchat::MenuAchat()
{



}

MenuAchat::MenuAchat(int fenX, int fenY)
{
    ma_estVisible=false;
    ma_fond=sf::RectangleShape(sf::Vector2f(fenX,fenY/5));
    ma_fond.setFillColor(sf::Color(255,255,255,120));
    ma_fond.setPosition(0,fenY-ma_fond.getSize().y);
    ma_croix=sf::RectangleShape(sf::Vector2f(ma_fond.getSize().y/5,ma_fond.getSize().y/5));
    ma_croix.setFillColor(sf::Color::Red);
    ma_croix.setPosition(fenX-ma_croix.getSize().x,ma_fond.getPosition().y-ma_croix.getSize().y);
}

MenuAchat::~MenuAchat()
{

}

void MenuAchat::drawIcon(sf::RenderWindow& fen)
{

    int i;
    if(ma_type=='T')
    {
        for(i=0;i<5;i++)
        {
            ma_iconesTourelles[i].spriteHolder=sf::RectangleShape(sf::Vector2f(8*ma_fond.getSize().y/10,8*ma_fond.getSize().y/10));//8/10 du fond
            ma_iconesTourelles[i].spriteHolder.setPosition(i*(ma_iconesTourelles[i].spriteHolder.getSize().x+ma_fond.getSize().x/25)+ma_fond.getSize().x/25,ma_fond.getPosition().y+ma_fond.getSize().y/10);
            ma_iconesTourelles[i].spriteHolder.setFillColor(sf::Color::Green);
            MenuAchat::draw(fen,ma_iconesTourelles[i].spriteHolder);


        }
    }
    else if(ma_type=='B')
    {
        for(i=0;i<5;i++)
        {
            ma_iconesBat[i].spriteHolder=sf::RectangleShape(sf::Vector2f(8*ma_fond.getSize().y/10,8*ma_fond.getSize().y/10));//8/10 du fond
            ma_iconesBat[i].spriteHolder.setPosition(i*(ma_iconesBat[i].spriteHolder.getSize().x+ma_fond.getSize().x/25)+ma_fond.getSize().x/25,ma_fond.getPosition().y+ma_fond.getSize().y/10);
            ma_iconesBat[i].spriteHolder.setFillColor(sf::Color::Cyan);
            MenuAchat::draw(fen,ma_iconesBat[i].spriteHolder);


        }
    }

}


void MenuAchat::draw(sf::RenderWindow& fen,sf::RectangleShape forme)
{
    fen.draw(forme);
}

void MenuAchat::update(sf::RenderWindow& fen)
{

   if(ma_estVisible)
   {
       MenuAchat::draw(fen,ma_croix);
       MenuAchat::draw(fen,ma_fond);
       MenuAchat::drawIcon(fen);
       //printf("\n UPDATE");
       /*if(ma_type=='T'){
       for(i=0;i<sizeof(ma_iconesTourelles)/sizeof(sf::RectangleShape);i++)
       {
           //printf("%i",i);
           MenuAchat::draw(fen,ma_iconesTourelles[i]);
       }
       }*/
   }
};

Batiment::Batiment()
{


}

Batiment::~Batiment()
{

}

Emplacement::Emplacement()
{

}

Emplacement::Emplacement(int num,char estConstruitType, int posx, int posy, sf::RectangleShape forme, bool estSurMur)//'N' pour vide
{
    e_estConstruitType=estConstruitType;
    e_posx=posx;
    e_posy=posy;
    e_forme=forme;
    e_estSurMur=estSurMur;
    e_num=num;

    if(!e_estSurMur)
    {
        e_forme.setPosition(posx,posy);
        e_forme.setFillColor(sf::Color::Yellow);

    }


}

Emplacement::Emplacement(char estConstruitType, int posx, int posy, sf::RectangleShape forme, bool estSurMur)//'N' pour vide
{
    e_estConstruitType=estConstruitType;
    e_posx=posx;
    e_posy=posy;
    e_forme=forme;
    e_estSurMur=estSurMur;


    if(!e_estSurMur)
    {
        e_forme.setPosition(posx,posy);
        e_forme.setFillColor(sf::Color::Yellow);

    }


}

Emplacement::~Emplacement()
{

}




bool Emplacement::estClicke()
{
    if(e_forme.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
    {
        return true;
    }
    else
        return false;
}

void Emplacement::draw(sf::RenderWindow& fen,sf::RectangleShape forme)
{
    fen.draw(forme);
}

Batiment::Batiment(int typeBat,int fenX, int fenY,sf::RenderWindow& fen)
{

    switch(typeBat){

    case(1):

            b_viemax=VIEMUR;
            b_vie=b_viemax;
            b_l= GRANDETAILLE;
            b_h=PETITETAILLE;
            b_posx=fenX/2-b_l/2;
            b_posy=fenY/2-b_l/2;
            b_forme=sf::RectangleShape(sf::Vector2f(b_l,b_h));
            b_forme.setPosition(b_posx,b_posy);

            b_barvie=sf::RectangleShape(sf::Vector2f(b_l/3,b_h/10));
            b_barviefond=sf::RectangleShape(sf::Vector2f(b_l/3,b_h/10));
            b_barvie.setPosition(b_posx+b_l/3,b_posy+b_h+b_h/5);
            b_barviefond.setPosition(b_posx+b_l/3,b_posy+b_h+b_h/5);
            break;



         case(2):

            b_viemax=VIEMUR;
            b_vie=b_viemax;
            b_l=PETITETAILLE;
            b_h=GRANDETAILLE;
            b_posx=(fenX/2+GRANDETAILLE/2-b_l);
            b_posy=fenY/2-GRANDETAILLE/2;
            b_forme=sf::RectangleShape(sf::Vector2f(b_l,b_h));
            b_forme.setFillColor(sf::Color::Cyan);
            b_forme.setPosition(b_posx,b_posy);

            b_barvie=sf::RectangleShape(sf::Vector2f(b_l/10,b_h/3));
            b_barviefond=sf::RectangleShape(sf::Vector2f(b_l/10,b_h/3));
            b_barvie.setPosition(b_posx-b_l/5,b_posy+b_h/3);
            b_barviefond.setPosition(b_posx-b_l/5,b_posy+b_h/3);

            break;



         case(3):

           b_viemax=VIEMUR;
            b_vie=b_viemax;
            b_l= GRANDETAILLE;
            b_h=PETITETAILLE;
            b_posx=fenX/2-b_l/2;
            b_posy=fenY/2+b_l/2-b_h;
            b_forme=sf::RectangleShape(sf::Vector2f(b_l,b_h));
            b_forme.setPosition(b_posx,b_posy);

            b_barvie=sf::RectangleShape(sf::Vector2f(b_l/3,b_h/10));
            b_barviefond=sf::RectangleShape(sf::Vector2f(b_l/3,b_h/10));
            b_barvie.setPosition(b_posx+b_l/3,b_posy-b_h/5);
            b_barviefond.setPosition(b_posx+b_l/3,b_posy-b_h/5);
            break;



         case(4):

            b_viemax=VIEMUR;
            b_vie=b_viemax;
            b_l=PETITETAILLE;
            b_h=GRANDETAILLE;
            b_posx=(fenX/2-GRANDETAILLE/2);
            b_posy=fenY/2-GRANDETAILLE/2;
            b_forme=sf::RectangleShape(sf::Vector2f(b_l,b_h));
            b_forme.setFillColor(sf::Color::Red);
            b_forme.setPosition(b_posx,b_posy);

            b_barvie=sf::RectangleShape(sf::Vector2f(b_l/10,b_h/3));
            b_barviefond=sf::RectangleShape(sf::Vector2f(b_l/10,b_h/3));
            b_barvie.setPosition(b_posx+b_l+b_l/5,b_posy+b_h/3);
            b_barviefond.setPosition(b_posx+b_l+b_l/5,b_posy+b_h/3);
            break;




        }

        b_barvie.setFillColor(sf::Color::Red);

        Batiment::getOrientation();
        int x=Batiment::getCoordonates().x;
        int y=Batiment::getCoordonates().y;

        printf("%i %i/",x,y);
        int i;
        Batiment::draw(fen,b_forme);
        //EMPLACEMENTS
        if(!Batiment::getOrientation())
        {
            for(i=0;i<3;i++){
            int zoneConstructible=b_l-2*b_h;
            int ecart =zoneConstructible/3/5;
            int taille=zoneConstructible/3-2*ecart;
            while(taille>b_h)
            {
                taille-=ecart;
            }

            if(taille<=0)
                taille=-taille;

           // Emplacement e={false,x,y,sf::RectangleShape(sf::Vector2f(taille,taille))};
            Emplacement e(false,x,y,sf::RectangleShape(sf::Vector2f(taille,taille)),true);
            Batiment::b_emplacements[i]=e;
            Batiment::b_emplacements[i].e_forme.setPosition((b_posx+b_h+ecart)+i*zoneConstructible/3,b_posy+(b_h-taille)/2);
            Batiment::b_emplacements[i].e_forme.setFillColor(sf::Color::Magenta);
            Batiment::draw(fen,Batiment::b_emplacements[i].e_forme);
            }
        }

            else
            {
                for(i=0;i<3;i++){
            int zoneConstructible=b_h-2*b_l;
            int ecart =zoneConstructible/3/5;
            int taille=zoneConstructible/3-2*ecart;

              while(taille>b_l)
            {
                taille-=ecart;
            }
            if(taille<=0)
                taille=-taille;

           // Emplacement e={false,x,y,sf::RectangleShape(sf::Vector2f(taille,taille))};
            Emplacement e(false,x,y,sf::RectangleShape(sf::Vector2f(taille,taille)),true);
            Batiment::b_emplacements[i]=e;
            Batiment::b_emplacements[i].e_forme.setPosition(b_posx+(b_l-taille)/2,(b_posy+b_l+ecart)+i*zoneConstructible/3);
            Batiment::b_emplacements[i].e_forme.setFillColor(sf::Color::Black);
            Batiment::draw(fen,Batiment::b_emplacements[i].e_forme);
            }
        }


}

void Batiment::draw(sf::RenderWindow& fen,sf::RectangleShape forme)
{
    fen.draw(forme);
}

void Batiment::recevoirDegats(int nbDegats)
{
    b_vie -= nbDegats;

    if (b_vie < 0)
    {
        b_vie = 0;
    }
    printf("yes i ");
    printf("%i\n",b_vie);
}

bool Batiment::estVivant()
{
    if (b_vie > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}


int Batiment::getOrientation()
{
    if(b_l>b_h)
        return 0;//horizontal
    else
        return 1;//vertical
}

sf::Vector2f Batiment::getCoordonates()
{
    float x=b_posx;
    float y=b_posy;
    return sf::Vector2f(x,y);
}

void Batiment::update(sf::RenderWindow& fen)
{
    int i;






    Batiment::draw(fen,b_forme);
    Batiment::draw(fen,b_barviefond);
    Batiment::draw(fen,b_barvie);
    if(Batiment::b_barviefond.getSize().x>Batiment::b_barviefond.getSize().y)//horizontal
        Batiment::b_barvie.setSize(sf::Vector2f(b_vie*b_barviefond.getSize().x/b_viemax,b_barviefond.getSize().y));
    if(Batiment::b_barviefond.getSize().x<Batiment::b_barviefond.getSize().y)//vertical
        Batiment::b_barvie.setSize(sf::Vector2f(b_barviefond.getSize().x,b_vie*b_barviefond.getSize().y/b_viemax));



    for(i=0;i<3;i++)
    {
        Batiment::draw(fen,Batiment::b_emplacements[i].e_forme);
    }

}

sf::Vector2f Batiment::getSize()
{
    int x=b_l;
    int y=b_h;
    return sf::Vector2f(x,y);
}


bool Batiment::estClicke()
{
    if(b_forme.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
    {
        return true;
    }
    else
        return false;
}

Mine::Mine(int posx, int posy, int taille)
{
    b_viemax=VIEMINE;
    b_vie=b_viemax;
    b_posx=posx;
    b_posy=posy;
    b_forme=sf::RectangleShape(sf::Vector2f(taille,taille));
    b_forme.setPosition(posx,posy);
    b_forme.setFillColor(sf::Color(152,22,65,255));
    b_barvie=sf::RectangleShape(sf::Vector2f(taille/3,taille/10));
    b_barviefond=sf::RectangleShape(sf::Vector2f(taille/3,taille/10));
    b_barvie.setPosition(b_posx+taille/3,b_posy+taille+taille/5);
    b_barviefond.setPosition(b_posx+taille/3,b_posy+taille+taille/5);
    b_barvie.setFillColor(sf::Color::Red);
    b_barviefond.setFillColor(sf::Color::White);
    m_clock=sf::Clock();
}
Mine::Mine()
{

}


int Mine::genererOr()
{
    int gold;
    printf("%f",m_clock.getElapsedTime().asSeconds());
    if(m_clock.getElapsedTime().asSeconds()>=5.0)
    {
        m_clock.restart();
        gold=ORMINE;
        printf("%i OR\n",gold);

    }
    else
    {
        gold=0;
    }

    return gold;
}


Fortification::Fortification()
{

}

Fortification::Fortification(int posx, int posy, int taille)
{
    b_viemax=VIEFORTIF;
    b_vie=b_viemax;
    b_posx=posx;
    b_posy=posy;
    b_forme=sf::RectangleShape(sf::Vector2f(taille,taille));
    b_forme.setPosition(posx,posy);
    b_forme.setFillColor(sf::Color(22,22,65,255));
    b_barvie=sf::RectangleShape(sf::Vector2f(taille/3,taille/10));
    b_barviefond=sf::RectangleShape(sf::Vector2f(taille/3,taille/10));
    b_barvie.setPosition(b_posx+taille/3,b_posy+taille+taille/5);
    b_barviefond.setPosition(b_posx+taille/3,b_posy+taille+taille/5);
    b_barvie.setFillColor(sf::Color::Red);
    b_barviefond.setFillColor(sf::Color::White);
}
