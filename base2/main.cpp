#include <SFML/Graphics.hpp>
#include <string.h>
#include "Batiment.hpp"
#include <string>
#include "personnage.hpp"
#include <ctime>
#define GUER "lib/guerrier.png"
#define ARCH "lib/archer.png"
#define MAGE "lib/mage.png"
#define TROL "lib/troll.png"
#define CAVA "lib/cavalier.png"
#define GOBE "lib/gobelin.png"
#define SQUE "lib/squelette.png"
#define DRAG "lib/dragon.png"
#define VAMP "lib/vampire.png"
#define GOLE "lib/golem.png"
#define NBMONSTRE 100
#define NBTYPEMONSTRE 10
#define TEMPSVAGUE 10+1
#define FRAMERATE 30

#include "tourelle.hpp"
#define IMGTOURELLE "imgTourelle2.png"
#define IMGCIBLE "lib/cible.png"


using namespace sf;
using namespace std;

bool tousMort(Personnage ennemis[ ],int longueur)
{
    int i;
    int compte=0;
    bool resultat;
    for(i=0; i<longueur; i++)
    {
        if(ennemis[i].visible==false)
            compte++;
    }
    if(compte>=longueur)
        resultat=true;
    else
        resultat=false;
    return resultat;
}



int main()
{
    int fenX=VideoMode::getDesktopMode().width;
    int fenY=VideoMode::getDesktopMode().height;


     srand(time(NULL));
      Clock clock;

    /**debut mathis**/
    float time;
    int ennemis[10]= {0};
    int i,j,k,t;
    int numVague=0;
    int tempsAvantVague;
    bool vagueActive=false;
    bool timerRelance=true;
    int goldTot=0;
    int totMonstre=NBMONSTRE*NBTYPEMONSTRE;
    Batiment* cible[15];





    RenderWindow fen(VideoMode(fenX,fenY),"jeu",Style::Fullscreen);


     fen.setFramerateLimit(FRAMERATE);
    time = clock.getElapsedTime().asSeconds();

     //Chargement d'une texture
    Texture guerrierT;
    if(!guerrierT.loadFromFile(GUER))
        printf("ERROR");
    Texture archerT;
    if(!archerT.loadFromFile(ARCH))
        printf("ERROR");
    Texture mageT;
    if(!mageT.loadFromFile(MAGE))
        printf("ERROR");
    Texture trollT;
    if(!trollT.loadFromFile(TROL))
        printf("ERROR");
    Texture cavalierT;
    if(!cavalierT.loadFromFile(CAVA))
        printf("ERROR");
    Texture gobelinT;
    if(!gobelinT.loadFromFile(GOBE))
        printf("ERROR");
    Texture squeletteT;
    if(!squeletteT.loadFromFile(SQUE))
        printf("ERROR");
    Texture dragonT;
    if(!dragonT.loadFromFile(DRAG))
        printf("ERROR");
    Texture vampireT;
    if(!vampireT.loadFromFile(VAMP))
        printf("ERROR");
    Texture golemT;
    if(!golemT.loadFromFile(GOLE))
        printf("ERROR");
    Font font;
    if(!font.loadFromFile(TEXT))
        printf("ERROR");

        //On d�finis un nombre de personnages que l'on va utiliser plus tard
    Personnage monstre[totMonstre];//!\ Utilis� pour tourelles
    //for(i=0;i<NBMONSTRE*NBTYPEMONSTRE+1;i++){
    for(j=0; j<NBTYPEMONSTRE; j++)
    {
        for(k=0; k<NBMONSTRE; k++)
        {
            Personnage personnage(j,fen,font);
                    switch(j)
                    {
                    case 0:
                        personnage.setTexture(guerrierT);
                        break;
                    case 1:
                        personnage.setTexture(archerT);
                        break;
                    case 2:
                        personnage.setTexture(mageT);
                        break;
                    case 3:
                        personnage.setTexture(trollT);
                        break;
                    case 4:
                        personnage.setTexture(cavalierT);
                        break;
                    case 5:
                        personnage.setTexture(gobelinT);
                        break;
                    case 6:
                        personnage.setTexture(squeletteT);
                        break;
                    case 7:
                        personnage.setTexture(dragonT);
                        break;
                    case 8:
                        personnage.setTexture(vampireT);
                        break;
                    case 9:
                        personnage.setTexture(golemT);
                        break;
                    }
            monstre[j*100+k]=personnage;

        }
    }

/** Fin Mathis **/

/** Debut nico **/
    Texture textureTourelles;
    if(!textureTourelles.loadFromFile(IMGTOURELLE))
        printf("erreur texture tourelles");


    Tourelle* tour[12];//PENSER A NEW
    for(i=0; i<=11; i++)//prends chaque tourelles 1 a 1
    {
       // Tourelle addTour(fen,textureTourelles,i*100,500);//!\ x et y a changer pour les mettre sur cube
        tour[i]=new Tourelle();// PENSER A NEW
    }


/** Fin nico debut mathis **/


    //int i,j;
    unsigned int gold=100000;

    Text goldscore;
    Text vagueText;
    Text vagueTimer;
    goldscore.setFont(font);
    vagueText.setFont(font);
    vagueTimer.setFont(font);
    char goldAffiche[20];
    char vagueAffiche[20];
    char timerAffiche[50];
    goldscore.setCharacterSize(50);
    vagueText.setCharacterSize(50);
    vagueTimer.setCharacterSize(50);
    vagueText.setPosition(fenX-250,0);
    vagueTimer.setPosition(fenX/2-vagueTimer.getGlobalBounds().width,0);

    Emplacement lastEmplacementHorsClic;
    Emplacement lastEmplacementMurClic;
    Batiment* murs[4];
    for(i=0; i<4; i++)
    {
        murs[i]=new Batiment(i+1,fenX,fenY,fen);

    }

    Mine* mines[12];
    Fortification* fortifs[12];
    //Cristal cristal = new cristal();
    Cristal* cristal= new Cristal(fenX,fenY);
    //int minesActives=0;

    for(i=0; i<12; i++)
    {
        mines[i]=new Mine();
        fortifs[i]= new Fortification();
    }

    int ecartMur = murs[0]->getSize().y/4;
    int tailleEmpl = murs[0]->getSize().x/3-murs[0]->getSize().x/6;

    Emplacement e1(0,murs[0]->getCoordonates().x,murs[0]->getCoordonates().y-ecartMur-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e2(1,murs[0]->getCoordonates().x+murs[0]->getSize().x/2-tailleEmpl/2,murs[0]->getCoordonates().y-ecartMur-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e3(2,murs[0]->getCoordonates().x+murs[0]->getSize().x-tailleEmpl,murs[0]->getCoordonates().y-ecartMur-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement e4(3,murs[1]->getCoordonates().x+murs[1]->getSize().x+ecartMur,murs[1]->getCoordonates().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e5(4,murs[1]->getCoordonates().x+murs[1]->getSize().x+ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y/2-tailleEmpl/2,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e6(5,murs[1]->getCoordonates().x+murs[1]->getSize().x+ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement e7(6,murs[0]->getCoordonates().x,murs[2]->getCoordonates().y+ecartMur+murs[0]->getSize().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e8(7,murs[0]->getCoordonates().x+murs[0]->getSize().x/2-tailleEmpl/2,murs[2]->getCoordonates().y+ecartMur+murs[0]->getSize().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e9(8,murs[0]->getCoordonates().x+murs[0]->getSize().x-tailleEmpl,murs[2]->getCoordonates().y+ecartMur+murs[0]->getSize().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement e10(9,murs[3]->getCoordonates().x-tailleEmpl-ecartMur,murs[1]->getCoordonates().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e11(10,murs[3]->getCoordonates().x-tailleEmpl-ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y/2-tailleEmpl/2,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e12(11,murs[3]->getCoordonates().x-tailleEmpl-ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement emplacementsHors[12]= {e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12};

    MenuAchat mAchat(fenX,fenY);



    fen.display();

    while(fen.isOpen())
    {
        if(cristal->b_vie<=0)
            break;
        Event event;
        while(fen.pollEvent(event))
        {
            switch(event.type)
            {
            case Event::Closed:

                fen.close();
                break;

            case Event::MouseButtonPressed:
                if(event.mouseButton.button==Mouse::Left)
                {
                     //printf("%c\n",lastEmplacementMurClic.e_estConstruitType);
                    //printf("%i\n",lastEmplacementMurClic.e_num);
                    //printf("\n%i",minesActives);
                    if(mAchat.ma_croix.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
                    {
                        //printf("yes i");
                        if(mAchat.ma_estVisible)
                        {
                            mAchat.ma_estVisible=false;
                        }
                    }

                    for(i=0; i<4; i++)
                    {

                       /* if(murs[i]->estClicke() )
                        {
                            murs[i]->recevoirDegats(250);
                        }*/


                        for(j=0; j<3; j++)
                        {

                            if(murs[i]->b_emplacements[j].estClicke())
                            {
                                if(mAchat.ma_type!='T' || !mAchat.ma_estVisible)
                                {
                                    mAchat.ma_type='T';
                                    mAchat.ma_estVisible=true;

                                }
                                lastEmplacementMurClic=murs[i]->b_emplacements[j];
                                //lastEmplacementMurClic.e_num=(i+1)*2+j-1;//obtenir num
                                if(i==0 && j==0)
                                    lastEmplacementMurClic.e_num=0;
                                if(i==0 && j==1)
                                    lastEmplacementMurClic.e_num=1;
                                if(i==0 && j==2)
                                    lastEmplacementMurClic.e_num=2;
                                if(i==1 && j==0)
                                    lastEmplacementMurClic.e_num=3;
                                if(i==1 && j==1)
                                    lastEmplacementMurClic.e_num=4;
                                if(i==1 && j==2)
                                    lastEmplacementMurClic.e_num=5;
                                if(i==2 && j==0)
                                    lastEmplacementMurClic.e_num=6;
                                if(i==2 && j==1)
                                    lastEmplacementMurClic.e_num=7;
                                if(i==2 && j==2)
                                    lastEmplacementMurClic.e_num=8;
                                if(i==3 && j==0)
                                    lastEmplacementMurClic.e_num=9;
                                if(i==3 && j==1)
                                    lastEmplacementMurClic.e_num=10;
                                if(i==3 && j==2)
                                    lastEmplacementMurClic.e_num=11;




                                //murs[i]->recevoirDegats(40);

                            }
                        }
                    }

                    for(i=0; i<12; i++)
                    {
                        if(emplacementsHors[i].estClicke())
                        {
                            if(mAchat.ma_type!='B' || !mAchat.ma_estVisible)
                            {
                                mAchat.ma_type='B';
                                mAchat.ma_estVisible=true;

                            }

                            if(lastEmplacementHorsClic.e_num!=emplacementsHors[i].e_num)
                            lastEmplacementHorsClic=emplacementsHors[i];

                            // printf("\n%i %i",lastEmplacementHorsClic.e_posx,lastEmplacementHorsClic.e_posy);
                        }



                        //pour tester
                        /*
                        if(mines[i]->estClicke())
                        {
                            mines[i]->recevoirDegats(25);
                        }

                        if(fortifs[i]->estClicke())
                        {
                            fortifs[i]->recevoirDegats(25);
                        }
                        */

                    }

                    //POUR ACHTER UN BAT
                    if(mAchat.ma_estVisible && mAchat.ma_type=='B')
                    {
                        for(i=0; i<2; i++)
                        {
                            if(mAchat.ma_iconesBat[i].spriteHolder.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
                            {
                                if(lastEmplacementHorsClic.e_estConstruitType!=mAchat.ma_iconesBat[i].i_type && gold>=mAchat.ma_iconesBat[i].cost)
                                {
                                    gold-=mAchat.ma_iconesBat[i].cost;
                                    //detruite batiment dja en place
                                    if(lastEmplacementHorsClic.e_estConstruitType!='N')
                                    {
                                        //printf("DCD");
                                        mines[lastEmplacementHorsClic.e_num]->recevoirDegats(1000);

                                        fortifs[lastEmplacementHorsClic.e_num]->recevoirDegats(1000);

                                    }

                                    switch(mAchat.ma_iconesBat[i].i_type)
                                    {


                                    case 'M':
                                        //printf("okkkkk");
                                        lastEmplacementHorsClic.e_estConstruitType='M';
                                        mines[lastEmplacementHorsClic.e_num]=new Mine(lastEmplacementHorsClic.e_posx,lastEmplacementHorsClic.e_posy,lastEmplacementHorsClic.e_forme.getSize().x);
                                        //minesActives++;
                                        //printf("\n%d",minesActives);

                                        break;


                                    case 'F':

                                        lastEmplacementHorsClic.e_estConstruitType='F';
                                        fortifs[lastEmplacementHorsClic.e_num]=new Fortification(lastEmplacementHorsClic.e_posx,lastEmplacementHorsClic.e_posy,lastEmplacementHorsClic.e_forme.getSize().x);
                                        //minesActives++;
                                        //printf("\n%d",minesActives);

                                        break;

                                    }

                                }

                            }
                        }
                    }




                     //POUR ACHTER UNE TOURELLE
                    if(mAchat.ma_estVisible && mAchat.ma_type=='T')
                    {
                        for(i=0; i<4; i++)
                        {
                            if(mAchat.ma_iconesTourelles[i].spriteHolder.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
                            {


                                if(lastEmplacementMurClic.e_estConstruitType!=mAchat.ma_iconesTourelles[i].i_type && gold>=mAchat.ma_iconesTourelles[i].cost)
                                {
                                    printf("%c", lastEmplacementMurClic.e_estConstruitType);
                                    printf("%c\n", mAchat.ma_iconesTourelles[i].i_type);




                                    switch(mAchat.ma_iconesTourelles[i].i_type)
                                    {


                                    case '0':
                                        printf("okkkkk");
                                        lastEmplacementMurClic.e_estConstruitType='0';
                                        tour[lastEmplacementMurClic.e_num]=new Tourelle(fen,textureTourelles,lastEmplacementMurClic.e_forme.getPosition().x,lastEmplacementMurClic.e_forme.getPosition().y);
                                        gold-=mAchat.ma_iconesTourelles[i].cost;

                                        break;

                                   case'1':

                                        if(lastEmplacementMurClic.e_estConstruitType=='0')
                                        {

                                            tour[lastEmplacementMurClic.e_num]->ajoutBonus(1);
                                            gold-=mAchat.ma_iconesTourelles[i].cost;
                                            mAchat.ma_iconesTourelles[i].cost+=15;

                                        }
                                        break;

                                    case'2':

                                        if(lastEmplacementMurClic.e_estConstruitType=='0')
                                        {

                                            tour[lastEmplacementMurClic.e_num]->ajoutBonus(2);
                                            gold-=mAchat.ma_iconesTourelles[i].cost;
                                            mAchat.ma_iconesTourelles[i].cost+=15;

                                        }
                                        break;

                                    case'3':

                                        if(lastEmplacementMurClic.e_estConstruitType=='0')
                                        {

                                            tour[lastEmplacementMurClic.e_num]->ajoutBonus(3);
                                            gold-=mAchat.ma_iconesTourelles[i].cost;
                                            mAchat.ma_iconesTourelles[i].cost+=15;

                                        }
                                        break;

                                    }

                                }

                            }
                        }
                    }








                    //FONCTION TEST\\ un click de souris permet de faire mal au unit�s
                    for(i=0; i<NBMONSTRE*NBTYPEMONSTRE; i++)
                    {
                        if(monstre[i].visible==true)
                            monstre[i].recevoirDegats(10);
                    }


                    printf("%c",lastEmplacementHorsClic.e_estConstruitType);
                            printf("%i\n",lastEmplacementHorsClic.e_num);


                }//finClick
                    break;
            }
        }


        //Pour qu'un personnage subisse des d�gats
        //attaquant.attaquer(cible)

        if(vagueActive==true)
        {
            numVague++;
            ennemis[0]=10*numVague;
            ennemis[1]=5*numVague;
            ennemis[2]=4*(numVague/3);
            ennemis[3]=4*(numVague/4);
            ennemis[4]=4*(numVague/5);
            ennemis[5]=10*(numVague/3);
            ennemis[6]=10*(numVague/6);
            ennemis[7]=2*(numVague/8);
            ennemis[8]=5*(numVague/7);
            ennemis[9]=(numVague/10);



            for(k=0; k<NBTYPEMONSTRE; k++)
            {
                for(i=0; i<ennemis[k]; i++)//ennemis= ennemis dans la vague
                {
                    for(j=0;j<4;j++)
                        {
                        cible[j]=murs[j];
                        }

                    monstre[i+NBMONSTRE*k].popPerso(cible);
                }
            }

            timerRelance=false;
            vagueActive=false;

        }
        else if(tousMort(monstre,totMonstre)==true && timerRelance==true)
        {
            time = clock.getElapsedTime().asSeconds();
            //printf("%i\n",time);
            if(time>=TEMPSVAGUE)
                vagueActive=true;
        }
        else if(tousMort(monstre,totMonstre)==true && timerRelance==false)
        {
            clock.restart();
            timerRelance=true;
            gold=gold+goldTot;
            goldTot=0;
        }




        //!\ code nico+mathis














         cristal->update(fen);

        for(i=0; i<4; i++)
        {
            if(!murs[i]->estVivant())
            {

                delete murs[i];
                murs[i]= new Batiment();
            }
            murs[i]->update(fen);

        }



        for(i=0; i<12; i++)
        {
            emplacementsHors[i].draw(fen,emplacementsHors[i].e_forme);




        }
        for(i=0; i<12; i++) //necessaire pour bon affichage
        {


            if(!mines[i]->estVivant())
            {

                delete mines[i];
              //  if(emplacementsHors[i].e_estConstruitType=='M')
                emplacementsHors[i].e_estConstruitType='N';
                mines[i]= new Mine();
            }


            if(!fortifs[i]->estVivant())
            {
                delete fortifs[i];
               // if(emplacementsHors[i].e_estConstruitType=='F')
                emplacementsHors[i].e_estConstruitType='N';
                fortifs[i]= new Fortification();
            }


            fortifs[i]->update(fen);
            mines[i]->update(fen);
            gold+=mines[i]->genererOr();
            //printf("%i\n",gold);



        }






        //On update les perso voulu pour les faire apparaitre
        for(i=0; i<totMonstre; i++)
        {
            for(t=0; t<=11; t++)
            {
                tour[t]->attaque(fen,monstre[i]);//prends tout les monstre et verifie si peut attaquer
            }
            if(monstre[i].mort==true)
            {
                goldTot=goldTot+monstre[i].goldWin;
                monstre[i].mort=false;
                //printf("Augmentation de gold : %i\n",goldTot);
            }

            //!// Definition de la personne que le monstre attaque
            monstre[i].cible=cristal;
            for(j=0;j<4;j++)
            {
                if(murs[j]->estVivant()==true)
                {
                    if(j+1==monstre[i].cote)
                        monstre[i].cible=murs[j];
                }
            }
            for(j=0;j<12;j++)
            {
                if(mines[j]->estVivant()==true)
                {
                    if(((j)/3)+1==monstre[i].cote)
                    {
                        if(j<3)
                            monstre[i].cible=mines[j];
                        else if(j<6)
                            monstre[i].cible=mines[j];
                        else if(j<9)
                            monstre[i].cible=mines[j];
                        else
                            monstre[i].cible=mines[j];
                    }
                }

            }
            for(j=0;j<12;j++)
            {
                if(fortifs[j]->estVivant()==true)
                {
                    if(((j)/3)+1==monstre[i].cote)
                    {
                        if(j<3)
                            monstre[i].cible=fortifs[j];
                        else if(j<6)
                            monstre[i].cible=fortifs[j];
                        else if(j<9)
                            monstre[i].cible=fortifs[j];
                        else
                            monstre[i].cible=fortifs[j];
                    }
                }

            }
            monstre[i].update(fen);
            //test.update(fen,test);
        }
        for(i=0; i<4; i++)//test murs detruit -> affiche pas tourelle et la desactive
        {
            if(murs[i]->estVivant()==false)
            {
                switch(i)
                {
                case 0:
                    tour[0]->visible=false;
                    tour[1]->visible=false;
                    tour[2]->visible=false;
                    break;
                case 1:
                    tour[3]->visible=false;
                    tour[4]->visible=false;
                    tour[5]->visible=false;
                    break;
                case 2:
                    tour[6]->visible=false;
                    tour[7]->visible=false;
                    tour[8]->visible=false;
                    break;
                case 3:
                    tour[9]->visible=false;
                    tour[10]->visible=false;
                    tour[11]->visible=false;
                    break;
                }

            }
        }

          for(i=0; i<=11; i++)//draw 12 tourelles
        {
            tour[i]->drawTourelle(fen);
        }

        mAchat.update(fen);

        tempsAvantVague=TEMPSVAGUE-time;

        sprintf(goldAffiche,"Or : %i",gold);
        sprintf(vagueAffiche,"Vague : %i",numVague);
        sprintf(timerAffiche,"Temps avant prochaine vague : %i",tempsAvantVague);
        vagueTimer.setPosition(fenX/2-vagueTimer.getGlobalBounds().width/2,0);
        goldscore.setString(goldAffiche);
        vagueText.setString(vagueAffiche);
        vagueTimer.setString(timerAffiche);
        fen.draw(goldscore);
        fen.draw(vagueText);
        fen.draw(vagueTimer);
        fen.display();
        fen.clear(sf::Color::Black);
    }


    return 0;
}
