#include <SFML/Graphics.hpp>
#include <ctime>
#include "personnage.hpp"
#include <string>
#define PERSO "lib/personnage.png"
#include <math.h>

using namespace sf;
Personnage::Personnage()
{

}

void Personnage::recevoirDegats(int nbDegats)
{
    vie=vie-nbDegats;
    if(vie<=0)
        vie=0;
    //printf("%i degat recus, vie: %i\n ",nbDegats,vie);
    Personnage::estVivant();
}
void Personnage::attaquer(Personnage &cible)
{
    Personnage::rechargement();
    //ciblage de la personne
    float xCible,yCible,xPerso,yPerso,x,y,xfen,yfen;
    xCible=cible.posX;
    yCible=cible.posY;
    xPerso=posX;
    yPerso=posY;
    xfen=(VideoMode::getDesktopMode().width)/2;
    yfen=(VideoMode::getDesktopMode().height)/2;
    x=(xCible-posDepX)/(xCible-cible.taille/2);
    y=(yCible-posDepY)/(xCible-cible.taille/2);
    if(cible.camps!=camps && cible.visible==true)
    {
        if (xCible-cible.taille/2-portee>=xPerso && yCible-cible.taille/2-portee>=yPerso)
        {
            Personnage::right(x);
            Personnage::down(y);
        }
        else if(xCible-cible.taille/2-portee>=xPerso && yCible+cible.taille/2+portee<=yPerso)
        {
            Personnage::right(x);
            Personnage::up(-y);
        }
        else if(xCible+cible.taille/2+portee<=xPerso && yCible-cible.taille/2-portee>=yPerso)
        {
            Personnage::left(-x);
            Personnage::down(y);
        }
        else if(xCible+cible.taille/2+portee<=xPerso && yCible+cible.taille/2+portee<=yPerso)
        {
            Personnage::left(-x);
            Personnage::up(-y);
        }
        else
        {

            if(xCible-cible.taille/2-portee>=xPerso)
            {
                Personnage::right(1.3);
            }
            else if(xCible+cible.taille/2+portee<=xPerso)
            {
                Personnage::left(1.3);
            }
            if(yCible-cible.taille/2-portee>=yPerso)
            {
                Personnage::down(1.3);
            }
            else if(yCible+cible.taille/2+portee<=yPerso)
            {
                Personnage::up(1.3);
            }
        }
    }

    //Recevoir d�gats
    if(posX<xfen && cible.visible==true && recharge==60*tempsRecharge)
    {
        if(posY<yfen)
        {

            if (xPerso+portee>=xCible-cible.taille/2 && yPerso+portee>=yCible-cible.taille/2)
            {
                cible.recevoirDegats(degats);
                recharge=0;
            }
        }
        else if(posY>yfen)
        {
            if (xPerso+portee>=xCible-cible.taille/2 && yPerso-portee<=yCible+cible.taille/2)
            {
                cible.recevoirDegats(degats);
                recharge=0;
            }
        }
    }
    else if(posX>xfen && cible.visible==true && recharge==60*tempsRecharge)
    {
        if(posY<yfen)
        {
            if (xPerso-portee<=xCible+cible.taille/2 && yPerso+portee>=yCible-cible.taille/2)
            {
                cible.recevoirDegats(degats);
                recharge=0;
            }
        }
        else if(posY>yfen)
        {

            if (xPerso-portee<=xCible+cible.taille/2 && yPerso-portee<=yCible+cible.taille/2)
            {
                cible.recevoirDegats(degats);
                recharge=0;
            }
        }
    }
}
void Personnage::rechargement()
{
    recharge++;
    if(recharge>=60*tempsRecharge)
        recharge=60*tempsRecharge;
}

void Personnage::estVivant()
{
    if(vie<=0)
    {
        visible=false;
    }
    else
    {
        visible=true;
    }
}
void afficheafficheScoreMort()
{

}

Personnage::Personnage(int typePerso, RenderWindow& fen, Texture& persoTexture)
{
    switch(typePerso)
    {
    case 0: //guerrier
        vie=100; //normal
        degats = 10; //normal
        vitesse = 1; //normal
        portee = 10; //corp � corp
        taille=50;//normal
        tempsRecharge=2;//normal
        score=10;//
        animeAttaque=1;//�p�e
        break;
    case 1: //archer
        vie=60; //reduit
        degats = 10; //normal
        vitesse = 1; //normal
        portee = 100; //distance
        taille=50;//normal
        tempsRecharge=2;//normal
        score=20;//
        animeAttaque=2;//arc
        break;
    case 2: //Mage
        vie=50; //reduit
        degats = 20; //normal
        vitesse = 1; //normal
        portee = 100; //distance
        taille=50;//normal
        tempsRecharge=3;//lent
        score=50;//
        animeAttaque=3;//boule de feu
        break;
    case 3: //Troll
        vie=120; //augment�
        degats = 20; //normal
        vitesse = 0.4; //lent
        portee = 10; //corp � corp
        taille=70;//�pais
        tempsRecharge=3;//lent
        score=80;//
        animeAttaque=1;//�p�e
        break;
    case 4: //Cavalier
        vie=100; //normal
        degats = 15; //augmnt�
        vitesse = 2; //rapide
        portee = 10; //c�c
        taille=50;//�pais
        tempsRecharge=2;//normal
        score=100;//
        animeAttaque=1;//�p�e
        break;
    case 5: //gobelin
        vie=20; //20
        degats = 5; //normal
        vitesse = 2.6; //tres rapide
        portee = 10; //corp � corp
        taille=25;//petit
        tempsRecharge=21;//rapide
        score=10;//
        animeAttaque=1;//�p�e
        break;
    case 6: //squelette
        vie=80; //normal
        degats = 5; //normal
        vitesse = 1; //normal
        portee = 10; //corp � corp//normal
        taille=25;//petit
        tempsRecharge=1;//rapide
        score=20;//
        animeAttaque=1;//�p�e
        break;
    case 7: //Dragon
        vie=200; //�lev�
        degats = 20; //normal
        vitesse = 0.4; //lent
        portee = 80; //petite distance
        taille=100;//Grand
        tempsRecharge=2;//normal
        score=150;//
        animeAttaque=3;//boule de feu
        break;
    case 8: //Vampire
        vie=80; //diminu�
        degats = 15; //augment�
        vitesse = 1; //normal
        portee = 10; //corp � corp
        taille=50;//Normal
        tempsRecharge=2;//normal
        score=50;//
        animeAttaque=4;//a la mano
        break;
    case 9: //Golem
        vie=500; //Tr�s �lev�
        degats = 50; //normal
        vitesse = 0.2; //tr�s lent
        portee = 10; //corp � corp
        taille=100;//Grand
        tempsRecharge=5;//Tres lent
        score=200;//
        animeAttaque=4;//a la mano
        break;
    }
    personnage.setTexture(persoTexture);
    personnage.setScale(taille/250,taille/250);
    visible=false;
    personnage.setOrigin(Vector2f(persoTexture.getSize().x/2,persoTexture.getSize().y/2));
    camps=1;
    vieMax=vie;

    barVie.setFillColor(Color::Green);
    barVieFond.setFillColor(Color::Red);
    barVieFond.setSize(Vector2f(vieMax,10));
    barVie.setSize(Vector2f(vie,barVieFond.getSize().y));
    barVieFond.setOrigin(Vector2f(vieMax/2,5));
}

void Personnage::update(RenderWindow& fen,Personnage &cibletest)
{
    if(visible==true)
    {
        personnage.setPosition(posX,posY);
        Personnage::attaquer(cibletest);
        barVie.setPosition(barVieFond.getPosition().x-vieMax/2,barVieFond.getPosition().y-5);
        barVieFond.setPosition(posX,posY-taille);
        Personnage::barVie.setSize(Vector2f(vie*barVieFond.getSize().x/vieMax,barVieFond.getSize().y));
        Personnage::draw(fen);
    }
}

void Personnage::up(float modif)
{
    posY=posY-vitesse*modif;
}
void Personnage::down(float modif)
{
    posY=posY+vitesse*modif;
}
void Personnage::left(float modif)
{
    posX=posX-vitesse*modif;
}
void Personnage::right(float modif)
{
    posX=posX+vitesse*modif;
}

void Personnage::popPerso(/*Batiment murs*/)
{
    if(visible==false)
    {

        int coteApparition=rand()%4+1;
        switch (coteApparition)
        {
        case 1: //haut
            posY=-100-rand()%200;
            posX=rand()%(VideoMode::getDesktopMode().width);
            //attaque(Batiment murs[0])
            break;
        case 2: //droite
            posY=rand()%(VideoMode::getDesktopMode().height);
            posX=(VideoMode::getDesktopMode().width)+100+rand()%200;
            //attaque(Batiment murs[1]
            break;
        case 3: //bas
            posY=(VideoMode::getDesktopMode().height)+100+rand()%200;
            posX=rand()%(VideoMode::getDesktopMode().width);
            //attaque(Batiment murs[2]
            break;
        case 4: //gauche
            posY=rand()%(VideoMode::getDesktopMode().height);
            posX=-100-rand()%200;
            //attaque(Batiment murs[3]
            break;
        }
        visible=true;
        posDepX=posX;
        posDepY=posY;
        vie=vieMax;
    }
}
void Personnage::forcePopPerso()
{

    if(visible==false)
    {
        posY=(VideoMode::getDesktopMode().height)/2;
        posX=(VideoMode::getDesktopMode().width)/2;
        visible=true;
        posDepX=posX;
        posDepY=posY;
    }

}

void Personnage::changementCamps()
{
    if(camps==1)
    {
        camps=0;
        personnage.setColor(Color::Green);
    }
    else if(camps==0)
    {
        camps=1;
        personnage.setColor(Color::Red);
    }
}

void Personnage::draw(RenderWindow& fen)
{
    fen.draw(personnage);
    if(vie<vieMax)
    {
        fen.draw(barVieFond);
        fen.draw(barVie);
    }
}


Personnage::~Personnage()
{

}

