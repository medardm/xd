#include <SFML/Graphics.hpp>
#include <string>
#include "personnage.hpp"
#include <ctime>
#define GUER "lib/guerrier.png"
#define ARCH "lib/archer.png"
#define MAGE "lib/mage.png"
#define TROL "lib/troll.png"
#define CAVA "lib/cavalier.png"
#define GOBE "lib/gobelin.png"
#define SQUE "lib/squelette.png"
#define DRAG "lib/dragon.png"
#define VAMP "lib/vampire.png"
#define GOLE "lib/golem.png"
#define NBMONSTRE 100
#define NBTYPEMONSTRE 10
#define TEMPSVAGUE 1
#define FRAMERATE 30

#include "tourelle.hpp"
#define IMGTOURELLE "imgTourelle2.png"
#define IMGCIBLE "lib/cible.png"


//METTRE UNE TAILLE AU PERSONNAGE POUR LE RETIRER
using namespace sf;
using namespace std;


bool tousMort(Personnage ennemis[ ])
{
    int tailleTab=sizeof(ennemis);
    int i;
    int compte=0;
    bool resultat;
    for(i=0; i<tailleTab; i++)
    {
        if(ennemis[i].visible==false)
            compte++;
    }
    if(compte>=tailleTab)
        resultat=true;
    else
        resultat=false;
    return resultat;
}


int main()
{
    //Apparition d'une fenetre


    srand(time(NULL));
    Event event;
    Clock clock;
/**debut mathis**/
    float time;
    int ennemis[10]= {0};
    int i,j,k,t;
    int numVague=0;
    bool vagueActive=false;
    bool timerRelance=true;

    int fenx = VideoMode::getDesktopMode().width;
    int feny = VideoMode::getDesktopMode().height;

    RenderWindow fen(VideoMode(fenx, feny), "GAME XD", Style::Fullscreen);
    fen.setFramerateLimit(FRAMERATE);
    time = clock.getElapsedTime().asSeconds();

    //Chargement d'une texture
    Texture guerrierT;
    if(!guerrierT.loadFromFile(GUER))
        printf("ERROR");
    Texture archerT;
    if(!archerT.loadFromFile(ARCH))
        printf("ERROR");
    Texture mageT;
    if(!mageT.loadFromFile(MAGE))
        printf("ERROR");
    Texture trollT;
    if(!trollT.loadFromFile(TROL))
        printf("ERROR");
    Texture cavalierT;
    if(!cavalierT.loadFromFile(CAVA))
        printf("ERROR");
    Texture gobelinT;
    if(!gobelinT.loadFromFile(GOBE))
        printf("ERROR");
    Texture squeletteT;
    if(!squeletteT.loadFromFile(SQUE))
        printf("ERROR");
    Texture dragonT;
    if(!dragonT.loadFromFile(DRAG))
        printf("ERROR");
    Texture vampireT;
    if(!vampireT.loadFromFile(VAMP))
        printf("ERROR");
    Texture golemT;
    if(!golemT.loadFromFile(GOLE))
        printf("ERROR");
    //On d�finis un nombre de personnages que l'on va utiliser plus tard
    Personnage monstre[NBMONSTRE*NBTYPEMONSTRE];//!\ Utilis� pour tourelles
    //for(i=0;i<NBMONSTRE*NBTYPEMONSTRE+1;i++){
    for(j=0; j<NBTYPEMONSTRE; j++)
    {
        for(k=0; k<NBMONSTRE; k++)
        {
            Personnage personnage(j,fen,guerrierT);
            monstre[j*100+k]=personnage;

        }
    }
    Personnage test(9,fen,golemT);

/** Fin Mathis **/

/** Debut nico **/
    Texture textureTourelles;
    if(!textureTourelles.loadFromFile(IMGTOURELLE))
        printf("erreur texture tourelles");


    Tourelle tour[12];
    for(i=0; i<=11; i++)//prends chaque tourelles 1 a 1
    {
        Tourelle addTour(fen,textureTourelles,i*100,500);//!\ x et y a changer pour les mettre sur cube
        tour[i]=addTour;
    }


/** Fin nico debut mathis **/

    test.forcePopPerso();
    test.changementCamps();


    //On d�marre le jeu
    while (fen.isOpen())
    {
        while (fen.pollEvent(event))
        {
            switch (event.type)
            {
            case Event::Closed:
                fen.close();
                break;
            case Event::MouseButtonPressed:
                if(event.mouseButton.button==Mouse::Left)
                {
                    for(i=0; i<NBMONSTRE*NBTYPEMONSTRE; i++)
                    {
                        if(monstre[i].visible==true)
                            monstre[i].recevoirDegats(10);
                    }
                }
                break;

            }

        }
        fen.clear();

        //Pour qu'un personnage subisse des d�gats
        //attaquant.attaquer(cible)
        if(vagueActive==true)
        {
            numVague++;
            ennemis[0]=10*numVague;
            ennemis[1]=5*numVague;
            ennemis[2]=4*(numVague/3);
            ennemis[3]=4*(numVague/4);
            ennemis[4]=4*(numVague/5);
            ennemis[5]=10*(numVague/3);
            ennemis[6]=10*(numVague/6);
            ennemis[7]=2*(numVague/8);
            ennemis[8]=5*(numVague/7);
            ennemis[9]=(numVague/10);



            for(k=0; k<NBTYPEMONSTRE; k++)
            {
                for(i=0; i<ennemis[k]; i++)//ennemis= ennemis dans la vague
                {
                    monstre[i+NBMONSTRE*k].popPerso();
                }
            }

            timerRelance=false;
            vagueActive=false;

        }
        else if(tousMort(monstre)==true && timerRelance==true)
        {
            time = clock.getElapsedTime().asSeconds();
            printf("%f\n",time);
            if(time>=TEMPSVAGUE)
                vagueActive=true;
        }
        else if(tousMort(monstre)==true && timerRelance==false)
        {
            clock.restart();
            timerRelance=true;
        }

 //!\ code nico+mathis

        //On update les perso voulu pour les faire apparaitre
        for(i=0; i<NBMONSTRE*NBTYPEMONSTRE; i++)
        {
            for(t=0; t<=11; t++)
            {
                tour[t].attaque(fen,monstre[i]);//prends tout les monstre et verifie si peut attaquer
            }

            monstre[i].update(fen,test);
            test.update(fen,test);
        }
        for(i=0; i<=11; i++)//draw 12 tourelles
        {
            tour[i].drawTourelle(fen);
        }
        fen.display();
    }

    return 0;
}

