
#include "fonctionMenu.hpp"


#define NOM_FOND "lib/fond.jpg"
#define NOM_CROIX "lib/trait.png"

#define TAILLE_TITRE 150
#define TAILLE_CARACTERES 50
#define POLICE "lib/ASMAN.ttf"

#define DEMI_LARGEUR_TITRE (titre.getLocalBounds().width)/2
#define DEMI_LARGEUR_JOUER (jouer.getLocalBounds().width)/2
#define DEMI_LARGEUR_REGLES (regles.getLocalBounds().width)/2
#define DEMI_LARGEUR_QUITTER (quitter.getLocalBounds().width)/2
#define DEMI_LARGEUR_REJOUER (rejouer.getLocalBounds().width)/2
#define RATIO_Y_TITRE 1.0/8.0
#define RATIO_Y_JOUER 4.0/8.0
#define RATIO_Y_REGLES 5.0/8.0
#define RATIO_Y_QUITTER 6.0/8.0

#define HAUTEUR_JOUER (jouer.getLocalBounds().height)
#define HAUTEUR_REGLES (regles.getLocalBounds().height)
#define HAUTEUR_QUITTER (quitter.getLocalBounds().height)
#define DEMI_HAUTEUR_QUITTER (quitter.getLocalBounds().height)/2
#define DEMI_HAUTEUR_REJOUER (rejouer.getLocalBounds().height)/2

#define FOND_L 1920
#define FOND_H 1080

using namespace sf;




void drawReplay(RenderWindow& fen, int dimx, int dimy, float pdimensionsTextes[10][10])
{
    // choix de la police � utiliser
    Font font;
    if (!font.loadFromFile(POLICE))
        printf("pb police");

//Creation Quitter//
    Text perdu;

    perdu.setFont(font);
    // choix de la phrase
    perdu.setString("Vous avez perdu...");
    // choix de la taille des caract�res
    perdu.setCharacterSize(TAILLE_TITRE);
    // choix du style du texte
    perdu.setStyle(Text::Bold);

    perdu.setPosition(0,0);

    perdu.setColor(Color(255,223,0));

    fen.draw(perdu);

    Text quitter;

    quitter.setFont(font);
    // choix de la phrase
    quitter.setString("Quitter");
    // choix de la taille des caract�res
    quitter.setCharacterSize(TAILLE_TITRE);
    // choix du style du texte
    quitter.setStyle(Text::Bold);

    quitter.setPosition(dimx/4-DEMI_LARGEUR_QUITTER,dimy/2-DEMI_HAUTEUR_QUITTER);

    quitter.setColor(Color(115,8,0));

    fen.draw(quitter);
    pdimensionsTextes[3][0]=DEMI_LARGEUR_QUITTER;
    pdimensionsTextes[3][1]=DEMI_HAUTEUR_QUITTER;

//Creation texte "jouer"//

    Text rejouer;

    rejouer.setFont(font);
    // choix de la phrase
    rejouer.setString("Rejouer");
    // choix de la taille des caract�res
    rejouer.setCharacterSize(TAILLE_TITRE);
    // choix du style du texte
    rejouer.setStyle(Text::Bold);

    rejouer.setPosition(dimx*3/4-DEMI_LARGEUR_REJOUER,dimy/2-DEMI_HAUTEUR_REJOUER);

    rejouer.setColor(Color(10, 145, 30));

    fen.draw(rejouer);
    pdimensionsTextes[4][0]=DEMI_LARGEUR_QUITTER;
    pdimensionsTextes[4][1]=DEMI_HAUTEUR_QUITTER;
}
void afficheRegles(RenderWindow& fen, int dimx, int dimy)
{
    // choix de la police � utiliser
    Font font;
    if (!font.loadFromFile(POLICE))
        printf("pb police");

//Creation regles//

    Text rules;

    rules.setFont(font);
    // choix de la phrase
    rules.setString("Regles:\n\t-Survivre aux vagues\n\t-Gagner de l'argent\n\t-Acheter des batiments et\n\ttourelles par le menu en bas du jeu");
    // choix de la taille des caract�res
    rules.setCharacterSize(TAILLE_CARACTERES);
    // choix du style du texte
    rules.setStyle(Text::Bold | Text::Italic);

    rules.setPosition(0,dimy*5/8);

    rules.setColor(Color(255,223,0));

    fen.draw(rules);
}

void afficheCredit(RenderWindow& fen, int dimx, int dimy)
{
    // choix de la police � utiliser
    Font font;
    if (!font.loadFromFile(POLICE))
        printf("pb police");

//Creation credit//

    Text credit;

    credit.setFont(font);
    // choix de la phrase
    credit.setString("CREDIT:\nMEDARD Mathis\nPACCOUD William\nWADOUX Nicolas");
    // choix de la taille des caract�res
    credit.setCharacterSize(TAILLE_CARACTERES);
    // choix du style du texte
    credit.setStyle(Text::Bold | Text::Italic);

    credit.setPosition(0,dimy*5/8);

    credit.setColor(Color(255,223,0));

    fen.draw(credit);
}
void drawFond(RenderWindow& fen, int dimx, int dimy)
{
    Texture imgFond ;
    if (!imgFond.loadFromFile(NOM_FOND))
        printf("PB de chargement de l'image \n");
    Sprite fond;
    fond.setTexture(imgFond);
    int PosSpriteX = dimx / 2 - FOND_L / 2 ;
    int PosSPriteY = dimy / 2 - FOND_H / 2 ;
    fond.setPosition(PosSpriteX,PosSPriteY);
    fen.draw(fond);
}

void drawAccueil(RenderWindow& fen, int dimx, int dimy, float pdimensionsTextes[10][10])
{
// choix de la police � utiliser
    Font font;
    if (!font.loadFromFile(POLICE))
        printf("pb police");

//Creation titre//

    Text titre;

    titre.setFont(font);
    // choix de la phrase
    titre.setString("Xtrem Defense");
    // choix de la taille des caract�res
    titre.setCharacterSize(TAILLE_TITRE);
    // choix du style du texte
    titre.setStyle(Text::Bold | Text::Underlined);

    titre.setPosition(dimx/2-DEMI_LARGEUR_TITRE,dimy*RATIO_Y_TITRE);

    titre.setColor(Color(115,8,0));

    fen.draw(titre);

//Creation texte "jouer"//

    Text jouer;

    jouer.setFont(font);
    // choix de la phrase
    jouer.setString("Jouer");
    // choix de la taille des caract�res
    jouer.setCharacterSize(TAILLE_CARACTERES);
    // choix du style du texte
    jouer.setStyle(Text::Bold);

    jouer.setPosition(dimx/2-DEMI_LARGEUR_JOUER,dimy*RATIO_Y_JOUER);

    fen.draw(jouer);

    pdimensionsTextes[0][0]=DEMI_LARGEUR_JOUER;
    pdimensionsTextes[0][1]=HAUTEUR_JOUER;


//Creation texte "regles"//

    Text regles;

    regles.setFont(font);
    // choix de la phrase
    regles.setString("Regles");
    // choix de la taille des caract�res
    regles.setCharacterSize(TAILLE_CARACTERES);
    // choix du style du texte
    regles.setStyle(Text::Bold);

    regles.setPosition(dimx/2-DEMI_LARGEUR_REGLES,dimy*RATIO_Y_REGLES);

    fen.draw(regles);

    pdimensionsTextes[1][0]=DEMI_LARGEUR_REGLES;
    pdimensionsTextes[1][1]=HAUTEUR_REGLES;

//Creation texte "quitter"//

    Text quitter;

    quitter.setFont(font);
    // choix de la phrase
    quitter.setString("Quitter");
    // choix de la taille des caract�res
    quitter.setCharacterSize(TAILLE_CARACTERES);
    // choix du style du texte
    quitter.setStyle(Text::Bold);

    quitter.setPosition(dimx/2-DEMI_LARGEUR_QUITTER,dimy*RATIO_Y_QUITTER);

    fen.draw(quitter);

    pdimensionsTextes[2][0]=DEMI_LARGEUR_QUITTER;
    pdimensionsTextes[2][1]=HAUTEUR_QUITTER;
}

void creerTraitSelect(RenderWindow& fen, int indice,int longeur, int hauteur,int dimx,int dimy)
{
    Texture cross;
    if (!cross.loadFromFile(NOM_CROIX))
        printf("PB de chargement de l'image de la croix de selection \n");

    RectangleShape selection(Vector2f(longeur*2,hauteur));

    if (indice==0)//play
    {
        selection.setPosition(dimx/2-longeur,dimy*RATIO_Y_JOUER);
    }
    else if (indice==1)//Regles
    {
        selection.setPosition(dimx/2-longeur,dimy*RATIO_Y_REGLES);
    }
    else if (indice==2)//Quitter
    {
        selection.setPosition(dimx/2-longeur,dimy*RATIO_Y_QUITTER);
    }
    else if (indice==3)//rejouer
    {
        selection.setPosition(dimx/4-longeur,dimy/2);
    }
    else if (indice==4)//quitter dans le menu perdu
    {
        selection.setPosition(dimx*3/4-longeur,dimy/2);
    }

    selection.setTexture(&cross);
    fen.draw(selection);

}
