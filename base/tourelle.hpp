#include "personnage.hpp"


using namespace sf;

class Tourelle
{
public:

    Tourelle();
    Tourelle(RenderWindow& fen,Texture& textureTourelle,int posX,int posY);
    ~Tourelle(); //destructeur pour le suppr au besoin
    void drawTourelle(RenderWindow& fen);
    void attaque(RenderWindow& fen,Personnage& ennemi);
    void rechargement();
    float tailleTourelle;
    int xPosT;
    int yPosT;
    bool visible;
    void ajoutBonus(int bonus);
    Sprite tourelle;

private:

    int degats;
    int bonusDegats;
    int portee;
    int bonusPortee;
    float recharge;
    int bonusRecharge;
    int type;
    float tpsRechargement;
};



