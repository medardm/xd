#include <SFML/Graphics.hpp>

using namespace sf;
void creerTraitSelect(RenderWindow& fen, int indice,int longeur, int hauteur,int dimx,int dimy);
void drawAccueil(RenderWindow& fen, int dimx, int dimy, float pdimensionsTextes[10][10]);
void drawFond(RenderWindow& fen, int dimx, int dimy);
void afficheCredit(RenderWindow& fen, int dimx, int dimy);
void afficheRegles(RenderWindow& fen, int dimx, int dimy);
