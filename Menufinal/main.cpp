#include <SFML/Graphics.hpp>
#include "fonctionMenu.hpp"

using namespace sf;
using namespace std;

/**Define de l'accueil**/
#define RATIO_Y_JOUER 4.0/8.0
#define RATIO_Y_REGLES 5.0/8.0
#define RATIO_Y_QUITTER 6.0/8.0

#define ZONE_JOUER (( event.mouseButton.x>fenx/2.0-dimensionsTextes[0][0] && event.mouseButton.x<fenx/2.0+dimensionsTextes[0][0] ) && ( event.mouseButton.y > feny*RATIO_Y_JOUER && event.mouseButton.y <= feny*RATIO_Y_JOUER+dimensionsTextes[0][1] ))
#define ZONE_REGLES (( event.mouseButton.x>fenx/2.0-dimensionsTextes[1][0] && event.mouseButton.x<fenx/2.0+dimensionsTextes[1][0] ) && ( event.mouseButton.y > feny*RATIO_Y_REGLES && event.mouseButton.y <= feny*RATIO_Y_REGLES+dimensionsTextes[1][1] ))
#define ZONE_QUITTER (( event.mouseButton.x>fenx/2.0-dimensionsTextes[2][0] && event.mouseButton.x<fenx/2.0+dimensionsTextes[2][0] ) && ( event.mouseButton.y > feny*RATIO_Y_QUITTER && event.mouseButton.y <= feny*RATIO_Y_QUITTER+dimensionsTextes[2][1] ))

#define SUR_JOUER (( event.mouseMove.x>fenx/2-dimensionsTextes[0][0] && event.mouseMove.x<fenx/2.0+dimensionsTextes[0][0] ) && ( event.mouseMove.y > feny*RATIO_Y_JOUER && event.mouseMove.y < feny*RATIO_Y_JOUER+dimensionsTextes[0][1] ))
#define SUR_REGLES (( event.mouseMove.x>fenx/2-dimensionsTextes[1][0] && event.mouseMove.x<fenx/2.0+dimensionsTextes[1][0] ) && ( event.mouseMove.y > feny*RATIO_Y_REGLES && event.mouseMove.y < feny*RATIO_Y_REGLES+dimensionsTextes[1][1] ))
#define SUR_QUITTER ((event.mouseMove.x>fenx/2-dimensionsTextes[2][0] && event.mouseMove.x<fenx/2.0+dimensionsTextes[2][0] ) && ( event.mouseMove.y > feny*RATIO_Y_QUITTER && event.mouseMove.y < feny*RATIO_Y_QUITTER+dimensionsTextes[2][1] ))

int main()
{

    /****PARTIE ACCUEIL****/

    float dimensionsTextes [10][10];// permet de recup les dimensions des textes pour les zones de clique 1-jouer 2-regles 3-quit // 0-x 1-y

    int fenx = VideoMode::getDesktopMode().width;
    int feny = VideoMode::getDesktopMode().height;//permet le full screen

    RenderWindow fen(VideoMode(fenx,feny), "SFML window",Style::Fullscreen);

    fen.setFramerateLimit(60);
    int jouer=0;
    bool Credit=false;
    int regles=1;

    // Loop accueil
    while (fen.isOpen() && jouer==0)
    {
        fen.clear();
        drawFond(fen, fenx, feny);
        drawAccueil(fen, fenx, feny,dimensionsTextes);

        Event event;

        while (fen.pollEvent(event))
        {
            if (event.type == Event::MouseButtonPressed)
            {


                if(event.mouseButton.button == Mouse::Right)//Afficher les credits
                {
                    Credit=true;
                }
                else if(ZONE_QUITTER)
                {
                    fen.close();
                }
                else if (ZONE_JOUER)
                {
                    jouer=1;
                    break;
                }
                else if (ZONE_REGLES)
                {
                    regles++;
                }
            }
            if (event.type == Event::MouseButtonReleased)
            {
                Credit=false;
            }

        }


        if (Credit)
        {
            afficheCredit(fen,fenx,feny);
        }

        if(regles%2 == 0)
        {
            afficheRegles(fen,fenx,feny);
        }

        if (SUR_QUITTER)
        {

            creerTraitSelect(fen,2,dimensionsTextes[2][0],dimensionsTextes[2][1],fenx,feny);//indice de deux veut dire "quitter"
        }
        else if (SUR_REGLES)
        {
            creerTraitSelect(fen,1,dimensionsTextes[1][0],dimensionsTextes[1][1],fenx,feny);
        }
        else if (SUR_JOUER)
        {
            creerTraitSelect(fen,0,dimensionsTextes[0][0],dimensionsTextes[0][1],fenx,feny);
        }
        fen.display();
    }


    return EXIT_SUCCESS;
}
