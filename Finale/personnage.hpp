#ifndef DEF_PERSONNAGE
#define DEF_PERSONNAGE
#include <string>
#include <SFML/Graphics.hpp>
#define TEXT "lib/ASMAN.ttf"
#include "Batiment.hpp"

class Personnage
{
public:

    Personnage();
    Personnage(int typePerso, sf::RenderWindow& fen,sf::Font& font);
    ~Personnage();
    void recevoirDegats(int nbDegats);
    void attaquer(Personnage &cible);
    void attaquer(Batiment* murs);
    void rechargement();
    void estVivant();
    void draw(sf::RenderWindow& fen);
    void setTexture(sf::Texture& persoTexture);
    void up(float modif);
    void down(float modif);
    void left(float modif);
    void right(float modif);
    void popPerso(Batiment* murs[]);
    void forcePopPerso();
    void update(sf::RenderWindow& fen);
    void changementCamps();
    void setGoldMort(sf::Font& font);
    void drawGoldMort(sf::RenderWindow& fen);
    bool visible;
    float posX;
    float posY;
    int vie;
    int goldWin;
    bool mort;
    Batiment* cible;
    int cote;
    int tier;

private:

    int typePerso;
    int vieMax;
    float vitesse;
    int portee;
    int degats;
    int animeAttaque;
    int camps;
    int recharge;
    int tempsRecharge;
    int gold;
    sf::Sprite personnage;
    sf::Sprite animation;
    sf::RectangleShape barVie;
    sf::RectangleShape barVieFond;
    sf::Text goldText;
    sf::Font font;
    float taille;
    float posDepX;
    float posDepY;
    int tempsApresMort;
};

#endif
