#ifndef DEF_BATIMENT
#define DEF_BATIMENT
#include <string>
#include <SFML/Graphics.hpp>


       /* class Emplacement
        {
            public:

            Emplacement();
           // Emplacement(int num_e,int b_l, int b_h, int b_posx, int b_posy);
            void draw(sf::RenderWindow& fen);

            private:
            bool e_estConstruit;
            int e_taille;
            int e_posx;
            int e_posy;
            sf::RectangleShape e_forme;
        };
*/

/*typedef struct
{
    bool e_estConstruit;
    int e_posx;
    int e_posy;
    sf::RectangleShape e_forme;

}Emplacement;*/

typedef struct
{
    sf::RectangleShape spriteHolder;
    char i_type;

}Icone;

class MenuAchat
{
public:
    MenuAchat();
    MenuAchat(int fenX,int fenY);
    ~MenuAchat();
    void update(sf::RenderWindow& fen);
    void draw(sf::RenderWindow& fen, sf::RectangleShape);
    //sf::RectangleShape ma_iconesTourelles[5];
   // sf::RectangleShape ma_iconesBat[5];
    Icone ma_iconesTourelles[5];
    Icone ma_iconesBat[5];
    sf::RectangleShape ma_fond;
    sf::RectangleShape ma_croix;
    char ma_type;//B ou T
    bool ma_estVisible;
    void drawIcon(sf::RenderWindow& fen);
};




class Emplacement
{
public:
    Emplacement();
    Emplacement(char estConstruitType, int posx, int posy, sf::RectangleShape forme,bool estSurMur);
    Emplacement(int num,char estConstruitType, int posx, int posy, sf::RectangleShape forme,bool estSurMur);
    ~Emplacement();
    bool estClicke();
    int e_num;
    char e_estConstruitType;
    int e_posx;
    int e_posy;
    sf::RectangleShape e_forme;
    bool e_estSurMur;
    void draw(sf::RenderWindow& fen, sf::RectangleShape forme);
};


class Batiment
{
public:

    Batiment();
    Batiment(int typeBat,int fenX, int fenY,sf::RenderWindow& fen);
    ~Batiment();
    void recevoirDegats(int nbDegats);
    bool estVivant();
    void draw(sf::RenderWindow& fen, sf::RectangleShape forme);
    int getOrientation();
    sf::Vector2f getCoordonates();
    void update(sf::RenderWindow& fen);
    bool estClicke();
    sf::Vector2f getSize();
    Emplacement b_emplacements[3];

    protected:

    int b_viemax;
    int b_vie;
    int b_posx;
    int b_posy;
    int b_h;
    int b_l;
    sf::RectangleShape b_barvie;
    sf::RectangleShape b_barviefond;
    sf::RectangleShape b_forme;

};

class Mine : public Batiment
{
public:
    Mine();
    Mine(int posx, int posy, int taille);
    sf::Clock m_clock;
    int genererOr();
    //g�n�rer or;

};

class Fortification : public Batiment
{
    public:
    Fortification();
    Fortification(int posx, int posy, int taille);
};

#endif

