#include "Batiment.hpp"
#include <string.h>
#define GRANDETAILLE 200
#define PETITETAILLE 40
#define VIEMUR 1000
#define VIEMINE 100
#define VIEFORTIF 250
#define ORMINE 20
#define COSTMINE 175
#define COSTFORTIF 100
#define VIECRISTAL 1000
#define COSTTOUR1 125
#define COSTRECHARGE 35
#define COSTPORTEE  25
#define COSTDEGATS 50
#include "tourelle.hpp"

using namespace std;

MenuAchat::MenuAchat()
{



}

MenuAchat::MenuAchat(int fenX, int fenY)
{
    ma_estVisible=false;
    ma_fond=sf::RectangleShape(sf::Vector2f(fenX,fenY/5));
    ma_fond.setFillColor(sf::Color(255,255,255,120));
    ma_fond.setPosition(0,fenY-ma_fond.getSize().y);
    ma_croix=sf::RectangleShape(sf::Vector2f(ma_fond.getSize().y/5,ma_fond.getSize().y/5));
    ma_croix.setFillColor(sf::Color::Red);
    ma_croix.setPosition(fenX-ma_croix.getSize().x,ma_fond.getPosition().y-ma_croix.getSize().y);

    int i;
    for(i=0;i<2;i++)
    {
        ma_iconesBat[i].affichercost.setCharacterSize(15);
        ma_iconesBat[i].affichercost.setColor(sf::Color::White);
        ma_iconesBat[i].font.loadFromFile(TEXT);
        ma_iconesBat[i].affichercost.setFont( ma_iconesBat[i].font);
    }

    ma_iconesBat[0].i_type='M';
    ma_iconesBat[0].cost=COSTMINE;


    ma_iconesBat[1].i_type='F';
    ma_iconesBat[1].cost=COSTFORTIF;



    for(i=0;i<4;i++)
    {
        ma_iconesTourelles[i].affichercost.setCharacterSize(15);
        ma_iconesTourelles[i].affichercost.setColor(sf::Color::White);
        ma_iconesTourelles[i].font.loadFromFile(TEXT);
        ma_iconesTourelles[i].affichercost.setFont( ma_iconesTourelles[i].font);
    }

    ma_iconesTourelles[0].i_type='0';
    ma_iconesTourelles[0].cost=COSTTOUR1;


    ma_iconesTourelles[1].i_type='1';
    ma_iconesTourelles[1].cost=COSTRECHARGE;


    ma_iconesTourelles[2].i_type='2';
    ma_iconesTourelles[2].cost=COSTDEGATS;


    ma_iconesTourelles[3].i_type='3';
    ma_iconesTourelles[3].cost=COSTPORTEE;




}

MenuAchat::~MenuAchat()
{

}

void MenuAchat::drawIcon(sf::RenderWindow& fen)
{

    int i;
    if(ma_type=='T')
    {
        for(i=0;i<4;i++)
        {
            ma_iconesTourelles[i].spriteHolder=sf::RectangleShape(sf::Vector2f(8*ma_fond.getSize().y/10,8*ma_fond.getSize().y/10));//8/10 du fond
            ma_iconesTourelles[i].spriteHolder.setPosition(i*(ma_iconesTourelles[i].spriteHolder.getSize().x+ma_fond.getSize().x/25)+ma_fond.getSize().x/25,ma_fond.getPosition().y+ma_fond.getSize().y/10);
            ma_iconesTourelles[i].spriteHolder.setFillColor(sf::Color::Green);
            //
            sprintf(ma_iconesTourelles[i].strcost,"%i Or",ma_iconesTourelles[i].cost);//
            ma_iconesTourelles[i].affichercost.setString(ma_iconesTourelles[i].strcost);
            ma_iconesTourelles[i].affichercost.setPosition(ma_iconesTourelles[i].spriteHolder.getPosition().x,ma_iconesTourelles[i].spriteHolder.getPosition().y+ma_iconesTourelles[i].spriteHolder.getSize().y);
            fen.draw(ma_iconesTourelles[i].affichercost);

            MenuAchat::draw(fen,ma_iconesTourelles[i].spriteHolder);


        }
    }
    else if(ma_type=='B')
    {
        for(i=0;i<2;i++)
        {
            ma_iconesBat[i].spriteHolder=sf::RectangleShape(sf::Vector2f(8*ma_fond.getSize().y/10,8*ma_fond.getSize().y/10));//8/10 du fond
            ma_iconesBat[i].spriteHolder.setPosition(i*(ma_iconesBat[i].spriteHolder.getSize().x+ma_fond.getSize().x/25)+ma_fond.getSize().x/25,ma_fond.getPosition().y+ma_fond.getSize().y/10);
            ma_iconesBat[i].spriteHolder.setFillColor(sf::Color::Cyan);

            sprintf(ma_iconesBat[i].strcost,"%i Or",ma_iconesBat[i].cost);//
            ma_iconesBat[i].affichercost.setString(ma_iconesBat[i].strcost);
            ma_iconesBat[i].affichercost.setPosition(ma_iconesBat[i].spriteHolder.getPosition().x,ma_iconesBat[i].spriteHolder.getPosition().y+ma_iconesBat[i].spriteHolder.getSize().y);
            fen.draw(ma_iconesBat[i].affichercost);



            MenuAchat::draw(fen,ma_iconesBat[i].spriteHolder);


        }
    }

}


void MenuAchat::draw(sf::RenderWindow& fen,sf::RectangleShape forme)
{
    fen.draw(forme);
}

void MenuAchat::update(sf::RenderWindow& fen)
{



   if(ma_estVisible)
   {
       MenuAchat::draw(fen,ma_croix);
       MenuAchat::draw(fen,ma_fond);
       MenuAchat::drawIcon(fen);


   }
};

Batiment::Batiment()
{


}

Batiment::~Batiment()
{

}

Emplacement::Emplacement()
{

}

Emplacement::Emplacement(int num,int posx, int posy, sf::RectangleShape forme, bool estSurMur)//'N' pour vide
{
    e_estConstruitType='N';
    e_posx=posx;
    e_posy=posy;
    e_forme=forme;
    e_estSurMur=estSurMur;
    e_num=num;
    e_forme.setFillColor(sf::Color(255,255,255,1));
    e_forme.setOutlineColor(sf::Color::Red);
    e_forme.setOutlineThickness(1.5);

    if(!e_estSurMur)
    {
        e_forme.setPosition(posx,posy);
    e_forme.setFillColor(sf::Color(255,255,255,1));
    e_forme.setOutlineColor(sf::Color::Red);
    e_forme.setOutlineThickness(1.5);

    }


}

Emplacement::Emplacement(int posx, int posy, sf::RectangleShape forme, bool estSurMur)//'N' pour vide
{
    e_estConstruitType='N';
    e_posx=posx;
    e_posy=posy;
    e_forme=forme;
    e_estSurMur=estSurMur;
    e_forme.setFillColor(sf::Color(255,255,255,1));
    e_forme.setOutlineColor(sf::Color::Red);
    e_forme.setOutlineThickness(1.5);


    if(!e_estSurMur)
    {
        e_forme.setPosition(posx,posy);
    e_forme.setFillColor(sf::Color(255,255,255,1));
    e_forme.setOutlineColor(sf::Color::Red);
    e_forme.setOutlineThickness(1.5);

    }


}

Emplacement::~Emplacement()
{

}




bool Emplacement::estClicke()
{
    if(e_forme.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
    {
        return true;
    }
    else
        return false;
}

void Emplacement::draw(sf::RenderWindow& fen,sf::RectangleShape forme)
{
    fen.draw(forme);
}

Batiment::Batiment(int typeBat,int fenX, int fenY,sf::RenderWindow& fen,Texture* longue,Texture* haut)
{

    switch(typeBat){

    case(1):

            b_viemax=VIEMUR;
            b_vie=b_viemax;
            b_l= GRANDETAILLE;
            b_h=PETITETAILLE;
            b_posx=fenX/2-b_l/2;
            b_posy=fenY/2-b_l/2;
            b_forme=sf::RectangleShape(sf::Vector2f(b_l,b_h));
            b_forme.setPosition(b_posx,b_posy);
			b_forme.setTexture(longue);
            b_barvie=sf::RectangleShape(sf::Vector2f(b_l/3,b_h/10));
            b_barviefond=sf::RectangleShape(sf::Vector2f(b_l/3,b_h/10));
            b_barvie.setPosition(b_posx+b_l/3,b_posy+b_h+b_h/5);
            b_barviefond.setPosition(b_posx+b_l/3,b_posy+b_h+b_h/5);
            break;



         case(2):

            b_viemax=VIEMUR;
            b_vie=b_viemax;
            b_l=PETITETAILLE;
            b_h=GRANDETAILLE;
            b_posx=(fenX/2+GRANDETAILLE/2-b_l);
            b_posy=fenY/2-GRANDETAILLE/2;
            b_forme=sf::RectangleShape(sf::Vector2f(b_l,b_h));
            b_forme.setPosition(b_posx,b_posy);
			b_forme.setTexture(haut);
            b_barvie=sf::RectangleShape(sf::Vector2f(b_l/10,b_h/3));
            b_barviefond=sf::RectangleShape(sf::Vector2f(b_l/10,b_h/3));
            b_barvie.setPosition(b_posx-b_l/5,b_posy+b_h/3);
            b_barviefond.setPosition(b_posx-b_l/5,b_posy+b_h/3);

            break;



         case(3):

           b_viemax=VIEMUR;
            b_vie=b_viemax;
            b_l= GRANDETAILLE;
            b_h=PETITETAILLE;
            b_posx=fenX/2-b_l/2;
            b_posy=fenY/2+b_l/2-b_h;
            b_forme=sf::RectangleShape(sf::Vector2f(b_l,b_h));
            b_forme.setPosition(b_posx,b_posy);
            b_forme.setTexture(longue);

            b_barvie=sf::RectangleShape(sf::Vector2f(b_l/3,b_h/10));
            b_barviefond=sf::RectangleShape(sf::Vector2f(b_l/3,b_h/10));
            b_barvie.setPosition(b_posx+b_l/3,b_posy-b_h/5);
            b_barviefond.setPosition(b_posx+b_l/3,b_posy-b_h/5);
            break;



         case(4):

            b_viemax=VIEMUR;
            b_vie=b_viemax;
            b_l=PETITETAILLE;
            b_h=GRANDETAILLE;
            b_posx=(fenX/2-GRANDETAILLE/2);
            b_posy=fenY/2-GRANDETAILLE/2;
            b_forme=sf::RectangleShape(sf::Vector2f(b_l,b_h));
            b_forme.setPosition(b_posx,b_posy);
            b_forme.setTexture(haut);
            b_barvie=sf::RectangleShape(sf::Vector2f(b_l/10,b_h/3));
            b_barviefond=sf::RectangleShape(sf::Vector2f(b_l/10,b_h/3));
            b_barvie.setPosition(b_posx+b_l+b_l/5,b_posy+b_h/3);
            b_barviefond.setPosition(b_posx+b_l+b_l/5,b_posy+b_h/3);
            break;




        }

        b_barvie.setFillColor(sf::Color::Red);

        Batiment::getOrientation();
        int x=Batiment::getCoordonates().x;
        int y=Batiment::getCoordonates().y;

        printf("%i %i/",x,y);
        int i;
        Batiment::draw(fen,b_forme);
        //EMPLACEMENTS
        if(!Batiment::getOrientation())
        {
            for(i=0;i<3;i++){
            int zoneConstructible=b_l-2*b_h;
            int ecart =zoneConstructible/3/5;
            int taille=zoneConstructible/3-2*ecart;
            while(taille>b_h)
            {
                taille-=ecart;
            }

            if(taille<=0)
                taille=-taille;

           // Emplacement e={false,x,y,sf::RectangleShape(sf::Vector2f(taille,taille))};
            Emplacement e(x,y,sf::RectangleShape(sf::Vector2f(taille,taille)),true);
            Batiment::b_emplacements[i]=e;
            Batiment::b_emplacements[i].e_forme.setPosition((b_posx+b_h+ecart)+i*zoneConstructible/3,b_posy+(b_h-taille)/2);
            Batiment::b_emplacements[i].e_forme.setFillColor(sf::Color(200,200,200,100));
            Batiment::draw(fen,Batiment::b_emplacements[i].e_forme);
            }
        }

            else
            {
                for(i=0;i<3;i++){
            int zoneConstructible=b_h-2*b_l;
            int ecart =zoneConstructible/3/5;
            int taille=zoneConstructible/3-2*ecart;

              while(taille>b_l)
            {
                taille-=ecart;
            }
            if(taille<=0)
                taille=-taille;

           // Emplacement e={false,x,y,sf::RectangleShape(sf::Vector2f(taille,taille))};
            Emplacement e(x,y,sf::RectangleShape(sf::Vector2f(taille,taille)),true);
            Batiment::b_emplacements[i]=e;
            Batiment::b_emplacements[i].e_forme.setPosition(b_posx+(b_l-taille)/2,(b_posy+b_l+ecart)+i*zoneConstructible/3);
            Batiment::b_emplacements[i].e_forme.setFillColor(sf::Color(200,200,200,100));
            Batiment::draw(fen,Batiment::b_emplacements[i].e_forme);
            }
        }


}

void Batiment::draw(sf::RenderWindow& fen,sf::RectangleShape forme)
{
    fen.draw(forme);
}

void Batiment::recevoirDegats(int nbDegats)
{
    b_vie -= nbDegats;

    if (b_vie < 0)
    {
        b_vie = 0;
    }
}

bool Batiment::estVivant()
{
    if (b_vie > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}


int Batiment::getOrientation()
{
    if(b_l>b_h)
        return 0;//horizontal
    else
        return 1;//vertical
}

sf::Vector2f Batiment::getCoordonates()
{
    float x=b_posx;
    float y=b_posy;
    return sf::Vector2f(x,y);
}

void Batiment::update(sf::RenderWindow& fen)
{
    int i;






    Batiment::draw(fen,b_forme);


    if(b_vie!=b_viemax)
    {
    Batiment::draw(fen,b_barviefond);
    Batiment::draw(fen,b_barvie);
    }

    if(Batiment::b_barviefond.getSize().x>Batiment::b_barviefond.getSize().y)//horizontal
        Batiment::b_barvie.setSize(sf::Vector2f(b_vie*b_barviefond.getSize().x/b_viemax,b_barviefond.getSize().y));
    if(Batiment::b_barviefond.getSize().x<Batiment::b_barviefond.getSize().y)//vertical
        Batiment::b_barvie.setSize(sf::Vector2f(b_barviefond.getSize().x,b_vie*b_barviefond.getSize().y/b_viemax));




    for(i=0;i<3;i++)
    {
        Batiment::draw(fen,Batiment::b_emplacements[i].e_forme);
    }

}

sf::Vector2f Batiment::getSize()
{
    int x=b_l;
    int y=b_h;
    return sf::Vector2f(x,y);
}


bool Batiment::estClicke()
{
    if(b_forme.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
    {
        return true;
    }
    else
        return false;
}

Mine::Mine(int posx, int posy, int taille,Texture* textureMine)
{
    b_viemax=VIEMINE;
    b_vie=b_viemax;
    b_posx=posx;
    b_posy=posy;
    b_forme=sf::RectangleShape(sf::Vector2f(taille,taille));
    b_forme.setPosition(posx,posy);
    b_barvie=sf::RectangleShape(sf::Vector2f(taille/3,taille/10));
    b_barviefond=sf::RectangleShape(sf::Vector2f(taille/3,taille/10));
    b_barvie.setPosition(b_posx+taille/3,b_posy+taille+taille/5);
    b_barviefond.setPosition(b_posx+taille/3,b_posy+taille+taille/5);
    b_barvie.setFillColor(sf::Color::Red);
    b_barviefond.setFillColor(sf::Color::White);
    m_clock=sf::Clock();
    b_h=taille;
    b_l=taille;
    b_forme.setTexture(textureMine);

}
Mine::Mine()
{
    b_viemax=0;
    b_vie=b_viemax;
}


int Mine::genererOr()
{
    if(Mine::estVivant())
    {

    int gold;

    if(m_clock.getElapsedTime().asSeconds()>=1)
    {
        m_clock.restart();
        gold=ORMINE;

    }
    else
    {
        gold=0;
    }

    return gold;

    }
}


Fortification::Fortification()
{

}

Fortification::Fortification(int posx, int posy, int taille,sf::Texture* texture)
{
    b_viemax=VIEFORTIF;
    b_vie=b_viemax;
    b_posx=posx;
    b_posy=posy;
    b_forme=sf::RectangleShape(sf::Vector2f(taille,taille));
    b_forme.setPosition(posx,posy);
    b_barvie=sf::RectangleShape(sf::Vector2f(taille/3,taille/10));
    b_barviefond=sf::RectangleShape(sf::Vector2f(taille/3,taille/10));
    b_barvie.setPosition(b_posx+taille/3,b_posy+taille+taille/5);
    b_barviefond.setPosition(b_posx+taille/3,b_posy+taille+taille/5);
    b_barvie.setFillColor(sf::Color::Red);
    b_barviefond.setFillColor(sf::Color::White);
    b_h=taille;
    b_l=taille;
    b_forme.setTexture(texture);
}

Cristal::Cristal(int fenX, int fenY,Texture* texture)
{
    b_viemax=VIECRISTAL;
    b_vie=b_viemax;
    c_taille=2*(GRANDETAILLE-2*PETITETAILLE)/10;

    b_h=c_taille;
    b_l=c_taille;

    b_posx=fenX/2-c_taille/2;
    b_posy=fenY/2-c_taille/2;

    b_forme=sf::RectangleShape(sf::Vector2f(c_taille,c_taille));
    b_forme.setPosition(b_posx,b_posy);
    b_barvie=sf::RectangleShape(sf::Vector2f(c_taille/3,c_taille/10));
    b_barviefond=sf::RectangleShape(sf::Vector2f(c_taille/3,c_taille/10));
    b_barvie.setPosition(b_posx+c_taille/3,b_posy+c_taille+c_taille/5);
    b_barviefond.setPosition(b_posx+c_taille/3,b_posy+c_taille+c_taille/5);
    b_barvie.setFillColor(sf::Color::Red);
    b_barviefond.setFillColor(sf::Color::White);
    b_forme.setTexture(texture);
}


Cristal::~Cristal()
{

}


