#ifndef DEF_BATIMENT
#define DEF_BATIMENT
#include <string>
#include <SFML/Graphics.hpp>
#define TEXT "lib/ASMAN.ttf"


       /* class Emplacement
        {
            public:

            Emplacement();
           // Emplacement(int num_e,int b_l, int b_h, int b_posx, int b_posy);
            void draw(sf::RenderWindow& fen);

            private:
            bool e_estConstruit;
            int e_taille;
            int e_posx;
            int e_posy;
            sf::RectangleShape e_forme;
        };
*/

/*typedef struct
{
    bool e_estConstruit;
    int e_posx;
    int e_posy;
    sf::RectangleShape e_forme;

}Emplacement;*/

typedef struct
{
    sf::RectangleShape spriteHolder;
    char i_type;
    unsigned int cost;
    sf::Text affichercost;
    sf::Font font;
    char strcost[10];

}Icone;

class MenuAchat
{
public:
    MenuAchat();
    MenuAchat(int fenX, int fenY);
    ~MenuAchat();
    void update(sf::RenderWindow& fen);
    void draw(sf::RenderWindow& fen, sf::RectangleShape);
    //sf::RectangleShape ma_iconesTourelles[5];
   // sf::RectangleShape ma_iconesBat[5];
    Icone ma_iconesTourelles[4];
    Icone ma_iconesBat[2];
    sf::RectangleShape ma_fond;
    sf::RectangleShape ma_croix;
    char ma_type;//B ou T
    bool ma_estVisible;
    void drawIcon(sf::RenderWindow& fen);
};




class Emplacement
{
public:
    Emplacement();
    Emplacement(int posx, int posy, sf::RectangleShape forme,bool estSurMur);
    Emplacement(int num,int posx, int posy, sf::RectangleShape forme,bool estSurMur);
    ~Emplacement();
    bool estClicke();
    int e_num;
    char e_estConstruitType;
    int e_posx;
    int e_posy;
    sf::RectangleShape e_forme;
    bool e_estSurMur;
    void draw(sf::RenderWindow& fen, sf::RectangleShape forme);
};


class Batiment
{
public:

    Batiment();
    Batiment(int typeBat,int fenX, int fenY,sf::RenderWindow& fen,sf::Texture* texture1,sf::Texture* texture2);
    ~Batiment();
    void recevoirDegats(int nbDegats);
    bool estVivant();
    void draw(sf::RenderWindow& fen, sf::RectangleShape forme);
    int getOrientation();
    sf::Vector2f getCoordonates();
    void update(sf::RenderWindow& fen);
    bool estClicke();
    sf::Vector2f getSize();
    Emplacement b_emplacements[3];
    int b_posx;
    int b_posy;
    int b_h;
    int b_l;
    int b_vie;

    protected:

    int b_viemax;
    sf::RectangleShape b_barvie;
    sf::RectangleShape b_barviefond;
    sf::RectangleShape b_forme;

};

class Mine : public Batiment
{
public:
    Mine();
    Mine(int posx, int posy, int taille,sf::Texture* texture);
    sf::Clock m_clock;
    int genererOr();
    //g�n�rer or;

};

class Fortification : public Batiment
{
    public:
    Fortification();
    Fortification(int posx, int posy, int taille,sf::Texture* texture);
};

class Cristal : public Batiment
{
    public:
    Cristal(int fenX, int fenY,sf::Texture* texture);
    ~Cristal();
    private:
        int c_taille;
};

#endif

