#include <SFML/Graphics.hpp>
#include <string.h>
#include "Batiment.hpp"
#include <string>
#include "personnage.hpp"
#include <ctime>
#include "fonctionMenu.hpp"
#include "tourelle.hpp"

#define GUER "lib/guerrier.png"
#define ARCH "lib/archer.png"
#define MAGE "lib/mage.png"
#define TROL "lib/troll.png"
#define CAVA "lib/cavalier.png"
#define GOBE "lib/gobelin.png"
#define SQUE "lib/squelette.png"
#define DRAG "lib/dragon.png"
#define VAMP "lib/vampire.png"
#define GOLE "lib/golem.png"
#define NBMONSTRE 100
#define NBTYPEMONSTRE 10
#define TEMPSVAGUE 10+1
#define FRAMERATE 30


#define IMGTOURELLE "imgTourelle2.png"
#define IMGCIBLE "lib/cible.png"

/**Define de l'accueil**/
#define RATIO_Y_JOUER 4.0/8.0
#define RATIO_Y_REGLES 5.0/8.0
#define RATIO_Y_QUITTER 6.0/8.0

#define ZONE_JOUER (( event.mouseButton.x>fenx/2.0-dimensionsTextes[0][0] && event.mouseButton.x<fenx/2.0+dimensionsTextes[0][0] ) && ( event.mouseButton.y > feny*RATIO_Y_JOUER && event.mouseButton.y <= feny*RATIO_Y_JOUER+dimensionsTextes[0][1] ))
#define ZONE_REGLES (( event.mouseButton.x>fenx/2.0-dimensionsTextes[1][0] && event.mouseButton.x<fenx/2.0+dimensionsTextes[1][0] ) && ( event.mouseButton.y > feny*RATIO_Y_REGLES && event.mouseButton.y <= feny*RATIO_Y_REGLES+dimensionsTextes[1][1] ))
#define ZONE_QUITTER (( event.mouseButton.x>fenx/2.0-dimensionsTextes[2][0] && event.mouseButton.x<fenx/2.0+dimensionsTextes[2][0] ) && ( event.mouseButton.y > feny*RATIO_Y_QUITTER && event.mouseButton.y <= feny*RATIO_Y_QUITTER+dimensionsTextes[2][1] ))

#define SUR_JOUER (( event.mouseMove.x>fenx/2-dimensionsTextes[0][0] && event.mouseMove.x<fenx/2.0+dimensionsTextes[0][0] ) && ( event.mouseMove.y > feny*RATIO_Y_JOUER && event.mouseMove.y < feny*RATIO_Y_JOUER+dimensionsTextes[0][1] ))
#define SUR_REGLES (( event.mouseMove.x>fenx/2-dimensionsTextes[1][0] && event.mouseMove.x<fenx/2.0+dimensionsTextes[1][0] ) && ( event.mouseMove.y > feny*RATIO_Y_REGLES && event.mouseMove.y < feny*RATIO_Y_REGLES+dimensionsTextes[1][1] ))
#define SUR_QUITTER ((event.mouseMove.x>fenx/2-dimensionsTextes[2][0] && event.mouseMove.x<fenx/2.0+dimensionsTextes[2][0] ) && ( event.mouseMove.y > feny*RATIO_Y_QUITTER && event.mouseMove.y < feny*RATIO_Y_QUITTER+dimensionsTextes[2][1] ))


#define END_SUR_QUITTER ((event.mouseMove.x>fenx/4-dimensionsTextes[3][0] && event.mouseMove.x<fenx/4+dimensionsTextes[3][0] ) && ( event.mouseMove.y > feny/2-dimensionsTextes[3][1] && event.mouseMove.y < feny/2+dimensionsTextes[3][1] ))
#define SUR_REJOUER ((event.mouseMove.x>fenx*3/4-dimensionsTextes[4][0] && event.mouseMove.x<fenx*3/4+dimensionsTextes[4][0] ) && ( event.mouseMove.y > feny/2-dimensionsTextes[4][1] && event.mouseMove.y < feny/2+dimensionsTextes[4][1] ))

#define END_ZONE_QUITTER ((event.mouseButton.x>fenx/4-dimensionsTextes[3][0] && event.mouseButton.x<fenx/4+dimensionsTextes[3][0] ) && ( event.mouseButton.y > feny/2-dimensionsTextes[3][1] && event.mouseButton.y < feny/2+dimensionsTextes[3][1] ))
#define ZONE_REJOUER ((event.mouseButton.x>fenx*3/4-dimensionsTextes[4][0] && event.mouseButton.x<fenx*3/4+dimensionsTextes[4][0] ) && ( event.mouseButton.y > feny/2-dimensionsTextes[4][1] && event.mouseButton.y < feny/2+dimensionsTextes[4][1] ))

using namespace sf;
using namespace std;

bool tousMort(Personnage ennemis[ ],int longueur) //Permet de verifier si tous les ennemis sont mort pour changer de vague
{
    int i;
    int compte=0;
    bool resultat;
    for(i=0; i<longueur; i++)
    {
        if(ennemis[i].visible==false)
            compte++;
    }
    if(compte>=longueur)
        resultat=true;
    else
        resultat=false;
    return resultat;
}



int jouer()//lance le jeu
{
    int fenX=VideoMode::getDesktopMode().width;
    int fenY=VideoMode::getDesktopMode().height;//Recupere les tailles de la fenetre suivant les �crans


    srand(time(NULL));
    Clock clock;

    float time;
    int ennemis[10]= {0}; //Nb de perso dans la vague
    int i,j,k,t;
    int numVague=0;
    int tempsAvantVague;
    bool vagueActive=false;
    bool timerRelance=true;
    int goldTot=0;
    int totMonstre=NBMONSTRE*NBTYPEMONSTRE;
    Batiment* cible[15]; //definir les cibles pour les ennemis
    char goldAffiche[20];
    char vagueAffiche[20];
    char timerAffiche[50];
    unsigned int gold=1000;

    RenderWindow fen(VideoMode(fenX,fenY),"jeu",Style::Fullscreen);

    fen.setFramerateLimit(FRAMERATE);//Limite le nbr d'images/sec
    time = clock.getElapsedTime().asSeconds();

     //Chargement des textures//
    Texture guerrierT;
    if(!guerrierT.loadFromFile(GUER))
        printf("ERROR");
    Texture archerT;
    if(!archerT.loadFromFile(ARCH))
        printf("ERROR");
    Texture mageT;
    if(!mageT.loadFromFile(MAGE))
        printf("ERROR");
    Texture trollT;
    if(!trollT.loadFromFile(TROL))
        printf("ERROR");
    Texture cavalierT;
    if(!cavalierT.loadFromFile(CAVA))
        printf("ERROR");
    Texture gobelinT;
    if(!gobelinT.loadFromFile(GOBE))
        printf("ERROR");
    Texture squeletteT;
    if(!squeletteT.loadFromFile(SQUE))
        printf("ERROR");
    Texture dragonT;
    if(!dragonT.loadFromFile(DRAG))
        printf("ERROR");
    Texture vampireT;
    if(!vampireT.loadFromFile(VAMP))
        printf("ERROR");
    Texture golemT;
    if(!golemT.loadFromFile(GOLE))
        printf("ERROR");
    Font font;
    if(!font.loadFromFile(TEXT))
        printf("ERROR");

    Texture textureTourelles;
    if(!textureTourelles.loadFromFile(IMGTOURELLE))
        printf("erreur texture tourelles");


    Personnage monstre[totMonstre];//Tableau de tout les ennemis d'x classe

    for(j=0; j<NBTYPEMONSTRE; j++)//on ajoute les differentes textures aux classes
    {
        for(k=0; k<NBMONSTRE; k++)
        {
            Personnage personnage(j,fen,font);
                    switch(j)
                    {
                    case 0:
                        personnage.setTexture(guerrierT);
                        break;
                    case 1:
                        personnage.setTexture(archerT);
                        break;
                    case 2:
                        personnage.setTexture(mageT);
                        break;
                    case 3:
                        personnage.setTexture(trollT);
                        break;
                    case 4:
                        personnage.setTexture(cavalierT);
                        break;
                    case 5:
                        personnage.setTexture(gobelinT);
                        break;
                    case 6:
                        personnage.setTexture(squeletteT);
                        break;
                    case 7:
                        personnage.setTexture(dragonT);
                        break;
                    case 8:
                        personnage.setTexture(vampireT);
                        break;
                    case 9:
                        personnage.setTexture(golemT);
                        break;
                    }
            monstre[j*100+k]=personnage;
        }
    }

    Tourelle* tour[12];//renvoi a la classe tourelle
    for(i=0; i<=11; i++)//prends chaque tourelles 1 a 1
    {
        tour[i]=new Tourelle();// cr�ation des 12 tourelles
    }

//Creation des textes de l'interface//
    Text goldscore;
    Text vagueText;
    Text vagueTimer;
    goldscore.setFont(font);
    vagueText.setFont(font);
    vagueTimer.setFont(font);

    goldscore.setCharacterSize(50);
    vagueText.setCharacterSize(50);
    vagueTimer.setCharacterSize(50);
    vagueText.setPosition(fenX-250,0);
    vagueTimer.setPosition(fenX/2-vagueTimer.getGlobalBounds().width,0);


    Emplacement lastEmplacementHorsClic;
    Emplacement lastEmplacementMurClic;
    Batiment* murs[4];
    for(i=0; i<4; i++)
    {
        murs[i]=new Batiment(i+1,fenX,fenY,fen);

    }

    Mine* mines[12];
    Fortification* fortifs[12];

    Cristal* cristal= new Cristal(fenX,fenY);


    for(i=0; i<12; i++)
    {
        mines[i]=new Mine();
        fortifs[i]= new Fortification();
    }

    int ecartMur = murs[0]->getSize().y/4;
    int tailleEmpl = murs[0]->getSize().x/3-murs[0]->getSize().x/6;

    Emplacement e1(0,murs[0]->getCoordonates().x,murs[0]->getCoordonates().y-ecartMur-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e2(1,murs[0]->getCoordonates().x+murs[0]->getSize().x/2-tailleEmpl/2,murs[0]->getCoordonates().y-ecartMur-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e3(2,murs[0]->getCoordonates().x+murs[0]->getSize().x-tailleEmpl,murs[0]->getCoordonates().y-ecartMur-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement e4(3,murs[1]->getCoordonates().x+murs[1]->getSize().x+ecartMur,murs[1]->getCoordonates().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e5(4,murs[1]->getCoordonates().x+murs[1]->getSize().x+ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y/2-tailleEmpl/2,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e6(5,murs[1]->getCoordonates().x+murs[1]->getSize().x+ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement e7(6,murs[0]->getCoordonates().x,murs[2]->getCoordonates().y+ecartMur+murs[0]->getSize().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e8(7,murs[0]->getCoordonates().x+murs[0]->getSize().x/2-tailleEmpl/2,murs[2]->getCoordonates().y+ecartMur+murs[0]->getSize().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e9(8,murs[0]->getCoordonates().x+murs[0]->getSize().x-tailleEmpl,murs[2]->getCoordonates().y+ecartMur+murs[0]->getSize().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement e10(9,murs[3]->getCoordonates().x-tailleEmpl-ecartMur,murs[1]->getCoordonates().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e11(10,murs[3]->getCoordonates().x-tailleEmpl-ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y/2-tailleEmpl/2,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e12(11,murs[3]->getCoordonates().x-tailleEmpl-ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement emplacementsHors[12]= {e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12};

    MenuAchat mAchat(fenX,fenY);



    fen.display();

    while(fen.isOpen())
    {
        if(cristal->b_vie<=0)
            break;
        Event event;
        while(fen.pollEvent(event))
        {
            switch(event.type)
            {
            case Event::Closed:

                fen.close();
                break;

            case Event::MouseButtonPressed:
                if(event.mouseButton.button==Mouse::Left)
                {
                    if(mAchat.ma_croix.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
                    {
                        if(mAchat.ma_estVisible)
                        {
                            mAchat.ma_estVisible=false;
                        }
                    }

                    for(i=0; i<4; i++)
                    {
                        for(j=0; j<3; j++)
                        {
                            if(murs[i]->b_emplacements[j].estClicke())
                            {
                                if(mAchat.ma_type!='T' || !mAchat.ma_estVisible)
                                {
                                    mAchat.ma_type='T';
                                    mAchat.ma_estVisible=true;
                                }
                                lastEmplacementMurClic=murs[i]->b_emplacements[j];
                                if(i==0 && j==0)
                                    lastEmplacementMurClic.e_num=0;
                                if(i==0 && j==1)
                                    lastEmplacementMurClic.e_num=1;
                                if(i==0 && j==2)
                                    lastEmplacementMurClic.e_num=2;
                                if(i==1 && j==0)
                                    lastEmplacementMurClic.e_num=3;
                                if(i==1 && j==1)
                                    lastEmplacementMurClic.e_num=4;
                                if(i==1 && j==2)
                                    lastEmplacementMurClic.e_num=5;
                                if(i==2 && j==0)
                                    lastEmplacementMurClic.e_num=6;
                                if(i==2 && j==1)
                                    lastEmplacementMurClic.e_num=7;
                                if(i==2 && j==2)
                                    lastEmplacementMurClic.e_num=8;
                                if(i==3 && j==0)
                                    lastEmplacementMurClic.e_num=9;
                                if(i==3 && j==1)
                                    lastEmplacementMurClic.e_num=10;
                                if(i==3 && j==2)
                                    lastEmplacementMurClic.e_num=11;
                            }
                        }
                    }

                    for(i=0; i<12; i++)
                    {
                        if(emplacementsHors[i].estClicke())
                        {
                            if(mAchat.ma_type!='B' || !mAchat.ma_estVisible)
                            {
                                mAchat.ma_type='B';
                                mAchat.ma_estVisible=true;
                            }
                            if(lastEmplacementHorsClic.e_num!=emplacementsHors[i].e_num)
                            lastEmplacementHorsClic=emplacementsHors[i];
                        }
                    }

                    //POUR ACHTER UN BAT
                    if(mAchat.ma_estVisible && mAchat.ma_type=='B')
                    {
                        for(i=0; i<2; i++)
                        {
                            if(mAchat.ma_iconesBat[i].spriteHolder.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
                            {
                                if(lastEmplacementHorsClic.e_estConstruitType!=mAchat.ma_iconesBat[i].i_type && gold>=mAchat.ma_iconesBat[i].cost)
                                {
                                    gold-=mAchat.ma_iconesBat[i].cost;
                                    //detruite batiment dja en place
                                    if(lastEmplacementHorsClic.e_estConstruitType!='N')
                                    {
                                        mines[lastEmplacementHorsClic.e_num]->recevoirDegats(1000);

                                        fortifs[lastEmplacementHorsClic.e_num]->recevoirDegats(1000); //!\ ?

                                    }

                                    switch(mAchat.ma_iconesBat[i].i_type)
                                    {


                                    case 'M':

                                        lastEmplacementHorsClic.e_estConstruitType='M';
                                        mines[lastEmplacementHorsClic.e_num]=new Mine(lastEmplacementHorsClic.e_posx,lastEmplacementHorsClic.e_posy,lastEmplacementHorsClic.e_forme.getSize().x);
                                        break;


                                    case 'F':

                                        lastEmplacementHorsClic.e_estConstruitType='F';
                                        fortifs[lastEmplacementHorsClic.e_num]=new Fortification(lastEmplacementHorsClic.e_posx,lastEmplacementHorsClic.e_posy,lastEmplacementHorsClic.e_forme.getSize().x);
                                        break;

                                    }
                                }
                            }
                        }
                    }

                     //POUR ACHTER UNE TOURELLE
                    if(mAchat.ma_estVisible && mAchat.ma_type=='T')
                    {
                        for(i=0; i<4; i++)
                        {
                            if(mAchat.ma_iconesTourelles[i].spriteHolder.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
                            {
                                if(lastEmplacementMurClic.e_estConstruitType!=mAchat.ma_iconesTourelles[i].i_type && gold>=mAchat.ma_iconesTourelles[i].cost)
                                {
                                    switch(mAchat.ma_iconesTourelles[i].i_type)
                                    {

                                    case '0':
                                        printf("okkkkk");
                                        lastEmplacementMurClic.e_estConstruitType='0';
                                        tour[lastEmplacementMurClic.e_num]=new Tourelle(fen,textureTourelles,lastEmplacementMurClic.e_forme.getPosition().x,lastEmplacementMurClic.e_forme.getPosition().y);
                                        gold-=mAchat.ma_iconesTourelles[i].cost;

                                        break;

                                   case'1':
                                        if(tour[lastEmplacementMurClic.e_num]->visible==true)
                                        {
                                            tour[lastEmplacementMurClic.e_num]->ajoutBonus(1);
                                            gold-=mAchat.ma_iconesTourelles[i].cost;
                                            mAchat.ma_iconesTourelles[i].cost+=5;
                                        }
                                        break;

                                    case'2':
                                        if(tour[lastEmplacementMurClic.e_num]->visible==true)
                                        {
                                            tour[lastEmplacementMurClic.e_num]->ajoutBonus(2);
                                            gold-=mAchat.ma_iconesTourelles[i].cost;
                                            mAchat.ma_iconesTourelles[i].cost+=5;
                                        }
                                        break;

                                    case'3':
                                        if(tour[lastEmplacementMurClic.e_num]->visible==true)
                                        {
                                            tour[lastEmplacementMurClic.e_num]->ajoutBonus(3);
                                            gold-=mAchat.ma_iconesTourelles[i].cost;
                                            mAchat.ma_iconesTourelles[i].cost+=5;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }

                }//finClick
                    break;
            }
        }

        if(vagueActive==true)
        {
            numVague++;
            ennemis[0]=10*numVague;
            ennemis[1]=5*numVague;
            ennemis[2]=4*(numVague/3);
            ennemis[3]=4*(numVague/4);
            ennemis[4]=4*(numVague/5);
            ennemis[5]=10*(numVague/3);
            ennemis[6]=10*(numVague/6);
            ennemis[7]=2*(numVague/8);
            ennemis[8]=5*(numVague/7);
            ennemis[9]=(numVague/10);

            for(k=0; k<NBTYPEMONSTRE; k++)
            {
                for(i=0; i<ennemis[k]; i++)//ennemis= ennemis dans la vague
                {
                    for(j=0;j<4;j++)
                        {
                        cible[j]=murs[j];
                        }
                    monstre[i+NBMONSTRE*k].popPerso(cible);
                }
            }
            timerRelance=false;
            vagueActive=false;
        }
        else if(tousMort(monstre,totMonstre)==true && timerRelance==true)
        {
            time = clock.getElapsedTime().asSeconds();
            if(time>=TEMPSVAGUE)
                vagueActive=true;
        }
        else if(tousMort(monstre,totMonstre)==true && timerRelance==false)
        {
            clock.restart();
            timerRelance=true;
            gold=gold+goldTot;
            goldTot=0;
        }

        cristal->update(fen);

        for(i=0; i<4; i++)
        {
            if(!murs[i]->estVivant())
            {
                delete murs[i];
                murs[i]= new Batiment();
            }
            murs[i]->update(fen);
        }

        for(i=0; i<12; i++)
        {
            emplacementsHors[i].draw(fen,emplacementsHors[i].e_forme);
        }

        for(i=0; i<12; i++) //necessaire pour bon affichage
        {
            if(!mines[i]->estVivant())
            {
                delete mines[i];
                emplacementsHors[i].e_estConstruitType='N';
                mines[i]= new Mine();
            }


            if(!fortifs[i]->estVivant())
            {
                delete fortifs[i];
                emplacementsHors[i].e_estConstruitType='N';
                fortifs[i]= new Fortification();
            }

            fortifs[i]->update(fen);
            mines[i]->update(fen);
            gold+=mines[i]->genererOr();
        }






        //On update les perso voulu pour les faire apparaitre
        for(i=0; i<totMonstre; i++)
        {
            for(t=0; t<=11; t++)
            {
                tour[t]->attaque(fen,monstre[i]);//prends tout les monstre et verifie si peut attaquer
            }
            if(monstre[i].mort==true)
            {
                goldTot=goldTot+monstre[i].goldWin;
                monstre[i].mort=false;
            }

            //Definition de la personne que le monstre attaque
            monstre[i].cible=cristal;
            for(j=0;j<4;j++)
            {
                if(murs[j]->estVivant()==true)
                {
                    if(j+1==monstre[i].cote)
                        monstre[i].cible=murs[j];
                }
            }
            for(j=0;j<12;j++)
            {
                if(mines[j]->estVivant()==true)
                {
                    if(((j)/3)+1==monstre[i].cote)
                    {
                        if(j<3)
                            monstre[i].cible=mines[j];
                        else if(j<6)
                            monstre[i].cible=mines[j];
                        else if(j<9)
                            monstre[i].cible=mines[j];
                        else
                            monstre[i].cible=mines[j];
                    }
                }
            }
            for(j=0;j<12;j++)
            {
                if(fortifs[j]->estVivant()==true)
                {
                    if(((j)/3)+1==monstre[i].cote)
                    {
                        if(j<3)
                            monstre[i].cible=fortifs[j];
                        else if(j<6)
                            monstre[i].cible=fortifs[j];
                        else if(j<9)
                            monstre[i].cible=fortifs[j];
                        else
                            monstre[i].cible=fortifs[j];
                    }
                }

            }
            monstre[i].update(fen);
        }

        for(i=0; i<4; i++)//test murs detruit -> affiche pas tourelle et la desactive
        {
            if(murs[i]->estVivant()==false)
            {
                switch(i)
                {
                case 0:
                    tour[0]->visible=false;
                    tour[1]->visible=false;
                    tour[2]->visible=false;
                    break;
                case 1:
                    tour[3]->visible=false;
                    tour[4]->visible=false;
                    tour[5]->visible=false;
                    break;
                case 2:
                    tour[6]->visible=false;
                    tour[7]->visible=false;
                    tour[8]->visible=false;
                    break;
                case 3:
                    tour[9]->visible=false;
                    tour[10]->visible=false;
                    tour[11]->visible=false;
                    break;
                }
            }
        }

        for(i=0; i<=11; i++)//draw 12 tourelles
        {
            tour[i]->drawTourelle(fen);
        }

        mAchat.update(fen);

        tempsAvantVague=TEMPSVAGUE-time;

        sprintf(goldAffiche,"Or : %i",gold);
        sprintf(vagueAffiche,"Vague : %i",numVague);
        sprintf(timerAffiche,"Temps avant prochaine vague : %i",tempsAvantVague);
        vagueTimer.setPosition(fenX/2-vagueTimer.getGlobalBounds().width/2,0);
        goldscore.setString(goldAffiche);
        vagueText.setString(vagueAffiche);
        vagueTimer.setString(timerAffiche);
        fen.draw(goldscore);
        fen.draw(vagueText);
        fen.draw(vagueTimer);
        fen.display();
        fen.clear(sf::Color::Black);
    }


    return 0;
}
int acc()
{
    /****PARTIE ACCUEIL****/

    float dimensionsTextes [10][10];// permet de recup les dimensions des textes pour les zones de clique 1-jouer 2-regles 3-quit // 0-x 1-y

    int fenx = VideoMode::getDesktopMode().width;
    int feny = VideoMode::getDesktopMode().height;//permet le full screen

    RenderWindow fen(VideoMode(fenx,feny), "SFML window",Style::Fullscreen);

    fen.setFramerateLimit(FRAMERATE);

    bool Credit=false;
    int regles=1;
    int fin;

    // Loop accueil
    while (fen.isOpen())
    {
        fen.clear();
        drawFond(fen, fenx, feny);
        drawAccueil(fen, fenx, feny,dimensionsTextes);

        Event event;

        while (fen.pollEvent(event))
        {
            if (event.type == Event::MouseButtonPressed)
            {
                if(event.mouseButton.button == Mouse::Right)//Afficher les credits
                {
                    Credit=true;
                }
                else if(ZONE_QUITTER)
                {
                    fin=1;
                    fen.close();
                }
                else if (ZONE_JOUER)
                {
                    fin=0;
                    fen.close();
                }
                else if (ZONE_REGLES)
                {
                    regles++;
                }
            }
            if (event.type == Event::MouseButtonReleased)
            {
                Credit=false;
            }
        }

        if (Credit)
        {
            afficheCredit(fen,fenx,feny);
        }

        if(regles%2 == 0)
        {
            afficheRegles(fen,fenx,feny);
        }

        if (SUR_QUITTER)
        {

            creerTraitSelect(fen,2,dimensionsTextes[2][0],dimensionsTextes[2][1],fenx,feny);//indice de deux veut dire "quitter"
        }
        else if (SUR_REGLES)
        {
            creerTraitSelect(fen,1,dimensionsTextes[1][0],dimensionsTextes[1][1],fenx,feny);
        }
        else if (SUR_JOUER)
        {
            creerTraitSelect(fen,0,dimensionsTextes[0][0],dimensionsTextes[0][1],fenx,feny);
        }
        fen.display();
    }
    return fin;
}


int rejouer()
{
    float dimensionsTextes [10][10];
    int fenx = VideoMode::getDesktopMode().width;
    int feny = VideoMode::getDesktopMode().height;//permet le full screen
    int fin;

    RenderWindow fen(VideoMode(fenx,feny), "SFML window",Style::Fullscreen);
    fen.setFramerateLimit(FRAMERATE);

    while (fen.isOpen())
    {
        fen.clear();
        drawFond(fen, fenx, feny);//mets l'image de fond
        drawReplay(fen, fenx, feny,dimensionsTextes);//ecrit quitter et rejouer

        Event event;

        while (fen.pollEvent(event))
        {
            if (event.type == Event::MouseButtonPressed)
            {
                if(END_ZONE_QUITTER)//si clic dans sur le txt quitter
                {
                    fin=1;
                    fen.close();
                }
                else if (ZONE_REJOUER)//si clic dans sur le txt Rejouer
                {
                    fen.close();
                }
            }
        }

        if (END_SUR_QUITTER)//si souris sur le txt quitter
        {
            creerTraitSelect(fen,3,dimensionsTextes[3][0],dimensionsTextes[3][1],fenx,feny);//affiche trait rouge
        }
        else if (SUR_REJOUER)//si souris sur le txt rejouer
        {
            creerTraitSelect(fen,4,dimensionsTextes[4][0],dimensionsTextes[4][1],fenx,feny);//affiche trait rouge
        }
        fen.display();
    }
    return fin;
}

int main()
{
    while(1){
    if(acc()==1)
        break;
    jouer();
    if(rejouer()==1)
        break;
    }
}
