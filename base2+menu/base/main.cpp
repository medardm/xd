#include <SFML/Graphics.hpp>
#include <windows.h>
#include <string.h>
#include "Batiment.hpp"

using namespace sf;



int main()
{
    int fenX=GetSystemMetrics(SM_CXSCREEN);
    int fenY=GetSystemMetrics(SM_CYSCREEN);

    RenderWindow fen(VideoMode(fenX,fenY),"jeu",Style::Fullscreen);
    RectangleShape centre(Vector2f(1,1));
    centre.setPosition(fenX/2,fenY/2);
    fen.draw(centre);
    /*
        Batiment mur1(11,fenX,fenY,fen);
        Batiment mur2(12,fenX,fenY,fen);
        Batiment mur3(13,fenX,fenY,fen);
        Batiment mur4(14,fenX,fenY,fen);

        Batiment murs[]={mur1,mur2,mur3,mur4};
    */
    int i,j;
    unsigned int gold=0;
    Emplacement lastEmplacementHorsClic;
    Emplacement lastEmplacementMurClic;
    Batiment* murs[4];
    for(i=0; i<4; i++)
    {
        murs[i]=new Batiment(i+1,fenX,fenY,fen);

    }

    Mine* mines[12];
    Fortification* fortifs[12];
    //int minesActives=0;

    for(i=0; i<12; i++)
    {
        mines[i]=new Mine();
        fortifs[i]= new Fortification();
    }

    int ecartMur = murs[0]->getSize().y/4;
    int tailleEmpl = murs[0]->getSize().x/3-murs[0]->getSize().x/6;

    Emplacement e1(0,'N',murs[0]->getCoordonates().x,murs[0]->getCoordonates().y-ecartMur-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e2(1,'N',murs[0]->getCoordonates().x+murs[0]->getSize().x/2-tailleEmpl/2,murs[0]->getCoordonates().y-ecartMur-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e3(2,'N',murs[0]->getCoordonates().x+murs[0]->getSize().x-tailleEmpl,murs[0]->getCoordonates().y-ecartMur-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement e4(3,'N',murs[1]->getCoordonates().x+murs[1]->getSize().x+ecartMur,murs[1]->getCoordonates().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e5(4,'N',murs[1]->getCoordonates().x+murs[1]->getSize().x+ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y/2-tailleEmpl/2,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e6(5,'N',murs[1]->getCoordonates().x+murs[1]->getSize().x+ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement e7(6,'N',murs[0]->getCoordonates().x,murs[2]->getCoordonates().y+ecartMur+murs[0]->getSize().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e8(7,'N',murs[0]->getCoordonates().x+murs[0]->getSize().x/2-tailleEmpl/2,murs[2]->getCoordonates().y+ecartMur+murs[0]->getSize().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e9(8,'N',murs[0]->getCoordonates().x+murs[0]->getSize().x-tailleEmpl,murs[2]->getCoordonates().y+ecartMur+murs[0]->getSize().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement e10(9,'N',murs[3]->getCoordonates().x-tailleEmpl-ecartMur,murs[1]->getCoordonates().y,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e11(10,'N',murs[3]->getCoordonates().x-tailleEmpl-ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y/2-tailleEmpl/2,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);
    Emplacement e12(11,'N',murs[3]->getCoordonates().x-tailleEmpl-ecartMur,murs[1]->getCoordonates().y+murs[1]->getSize().y-tailleEmpl,sf::RectangleShape(Vector2f(tailleEmpl,tailleEmpl)),false);

    Emplacement emplacementsHors[12]= {e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12};

    MenuAchat mAchat(fenX,fenY);
    mAchat.ma_iconesBat[0].i_type='M';
    mAchat.ma_iconesBat[1].i_type='F';



    fen.display();

    while(fen.isOpen())
    {
        Event event;
        while(fen.pollEvent(event))
        {
            switch(event.type)
            {
            case Event::Closed:

                fen.close();
                break;

            case Event::MouseButtonPressed:
                if(event.mouseButton.button==Mouse::Left)
                {
                    //printf("\n%i",minesActives);
                    if(mAchat.ma_croix.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
                    {
                        printf("yes i");
                        if(mAchat.ma_estVisible)
                        {
                            mAchat.ma_estVisible=false;
                        }
                    }

                    for(i=0; i<4; i++)
                    {

                        if(murs[i]->estClicke())
                        {
                            murs[i]->recevoirDegats(250);
                        }


                        for(j=0; j<3; j++)
                        {

                            if(murs[i]->b_emplacements[j].estClicke())
                            {
                                if(mAchat.ma_type!='T' || !mAchat.ma_estVisible)
                                {
                                    mAchat.ma_type='T';
                                    mAchat.ma_estVisible=true;

                                }
                                lastEmplacementMurClic=murs[i]->b_emplacements[j];
                                //murs[i]->recevoirDegats(40);

                            }
                        }
                    }

                    for(i=0; i<12; i++)
                    {
                        if(emplacementsHors[i].estClicke())
                        {
                            if(mAchat.ma_type!='B' || !mAchat.ma_estVisible)
                            {
                                mAchat.ma_type='B';
                                mAchat.ma_estVisible=true;

                            }
                            lastEmplacementHorsClic=emplacementsHors[i];
                            // printf("\n%i %i",lastEmplacementHorsClic.e_posx,lastEmplacementHorsClic.e_posy);
                        }


                        if(mines[i]->estClicke())
                        {
                            mines[i]->recevoirDegats(25);
                        }

                        if(fortifs[i]->estClicke())
                        {
                            fortifs[i]->recevoirDegats(25);
                        }

                    }


                    if(mAchat.ma_estVisible && mAchat.ma_type=='B')
                    {
                        for(i=0; i<5; i++)
                        {
                            if(mAchat.ma_iconesBat[i].spriteHolder.getGlobalBounds().contains(sf::Mouse::getPosition().x,sf::Mouse::getPosition().y))
                            {
                                if(lastEmplacementHorsClic.e_estConstruitType!=mAchat.ma_iconesBat[i].i_type)
                                {

                                    if(lastEmplacementHorsClic.e_estConstruitType!='N')
                                    {
                                        printf("DCD");
                                        mines[lastEmplacementHorsClic.e_num]->recevoirDegats(1000);
                                        fortifs[lastEmplacementHorsClic.e_num]->recevoirDegats(1000);
                                    }

                                    switch(mAchat.ma_iconesBat[i].i_type)
                                    {


                                    case 'M':
                                        //printf("okkkkk");
                                        lastEmplacementHorsClic.e_estConstruitType='M';
                                        mines[lastEmplacementHorsClic.e_num]=new Mine(lastEmplacementHorsClic.e_posx,lastEmplacementHorsClic.e_posy,lastEmplacementHorsClic.e_forme.getSize().x);
                                        //minesActives++;
                                        //printf("\n%d",minesActives);

                                        break;


                                    case 'F':

                                        lastEmplacementHorsClic.e_estConstruitType='F';
                                        fortifs[lastEmplacementHorsClic.e_num]=new Fortification(lastEmplacementHorsClic.e_posx,lastEmplacementHorsClic.e_posy,lastEmplacementHorsClic.e_forme.getSize().x);
                                        //minesActives++;
                                        //printf("\n%d",minesActives);

                                        break;

                                    }

                                }

                            }
                        }
                    }





                }//finClick

            }
        }

        for(i=0; i<4; i++)
        {
            if(!murs[i]->estVivant())
            {

                delete murs[i];
                murs[i]= new Batiment();
            }
            murs[i]->update(fen);

        }

        for(i=0; i<12; i++)
        {
            emplacementsHors[i].draw(fen,emplacementsHors[i].e_forme);




        }
        for(i=0; i<12; i++) //necessaire pour bon affichage
        {


            if(!mines[i]->estVivant())
            {

                delete mines[i];
                emplacementsHors[i].e_estConstruitType='N';
                mines[i]= new Mine();
            }


            if(!fortifs[i]->estVivant())
            {
                delete fortifs[i];
                emplacementsHors[i].e_estConstruitType='N';
                fortifs[i]= new Fortification();
            }


            fortifs[i]->update(fen);
            mines[i]->update(fen);
            gold+=mines[i]->genererOr();
            //printf("%i\n",gold);



        }


        //int nbMinesDetruites;

        //detruire Mines
        /*for(i=0;i<minesActives;i++)
        {
            if(!mines[i]->estVivant())
                {
                    nbMinesDetruites++;
                    delete mines[i];
                    mines[i]= new Mine();
                    printf("\n DETRUITES %i",nbMinesDetruites);
                }
            mines[i]->update(fen);
        }
        minesActives-=nbMinesDetruites;
        if(minesActives<=0)
        {
            minesActives=0;
        }
        nbMinesDeruites=0;
        */





        mAchat.update(fen);
        fen.display();
        fen.clear(sf::Color::Black);
    }


    return 0;
}
