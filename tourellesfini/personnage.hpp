#ifndef DEF_PERSONNAGE
#define DEF_PERSONNAGE
#include <string>
#include <SFML/Graphics.hpp>

class Personnage
{
public:

    Personnage();
    Personnage(int typePerso, sf::RenderWindow& fen,sf::Texture& persoTexture);
    ~Personnage();
    void recevoirDegats(int nbDegats);
    void attaquer(Personnage &cible);
    void rechargement();
    void estVivant();
    void draw(sf::RenderWindow& fen);
    void up(float modif);
    void down(float modif);
    void left(float modif);
    void right(float modif);
    void popPerso();
    void forcePopPerso();
    void update(sf::RenderWindow& fen,Personnage &cibletest);
    void changementCamps();
    void afficheScoreMort();
    bool visible;
    int score;
    float posX;
    float posY;
    sf::RectangleShape barVie;
    sf::RectangleShape barVieFond;
    int vie;

private:

    int typePerso;
    int vieMax;
    float vitesse;
    int portee;
    int degats;
    int animeAttaque;
    int camps;
    int recharge;
    int tempsRecharge;
    sf::Sprite personnage;
    float taille;
    float posDepX;
    float posDepY;
};

#endif
