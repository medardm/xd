#include "personnage.hpp"


using namespace sf;

class Tourelle
{
public:

    Tourelle();
    Tourelle(RenderWindow& fen,Texture& textureTourelle,int posX,int posY);
    ~Tourelle(); //destructeur pour le suppr au besoin
    void drawTourelle(RenderWindow& fen);
    void changementType(int nouveauType);
    void attaque(RenderWindow& fen,Personnage& ennemi);
    void rechargement();
    float tailleTourelle;
    int xPosT;
    int yPosT;
    bool visible;


private:
    Sprite tourelle;
    int recharge;
    int type;
    int degats;
    int portee;
    int tpsRechargement;
};



