#include <SFML/Graphics.hpp>
#include "tourelle.hpp"
#include "personnage.hpp"

using namespace sf;

#define IMGTOURELLE "imgTourelle2.png"
#define TAILLE_TOURELLE 40/tourelle.getGlobalBounds().width

#define DEGATS_DEFAUT 25
#define PORTEE_DEFAUT 200
#define RECHARGE_DEFAUT 5*30 /*equivaut a 5 sec a cause du framerate de 30*/

#define REDUC_RECHARGE 0.2
#define BONUS_DEGATS 5
#define BONUS_PORTEE 20

#define MAX_VIE_ENNEMI 500
#define MAX_PORTEE 1000

void Tourelle::attaque(RenderWindow& fen,Personnage& ennemi)
{

    if (visible==true && ennemi.visible==true)
    {
        if((recharge>=RECHARGE_DEFAUT)&&(( (ennemi.posX > xPosT+TAILLE_TOURELLE/2-portee) && (ennemi.posX < xPosT+TAILLE_TOURELLE/2+portee)) && ((ennemi.posY > yPosT+TAILLE_TOURELLE/2-portee) && (ennemi.posY < yPosT+TAILLE_TOURELLE/2+portee))))
        {
            ennemi.recevoirDegats(degats);
            recharge=0;
        }

    }
}

void Tourelle::rechargement()
{
    recharge++;
    if(recharge>=tpsRechargement)
        recharge=tpsRechargement;
}

Tourelle::Tourelle()
{

}

Tourelle::Tourelle(RenderWindow& fen,Texture& textureTourelle,int posX,int posY)
{
    //manque : gestion clic / gestion monnaie
    textureTourelle.loadFromFile(IMGTOURELLE);
    tourelle.setTexture(textureTourelle);
    tailleTourelle=TAILLE_TOURELLE;
    tourelle.setScale(tailleTourelle,tailleTourelle);
    visible=true;/** gerer avec clic sur bat pour devenir true et false quand mur detruit**/
    xPosT=posX;
    yPosT=posY;
    tourelle.setPosition(posX,posY);
    tourelle.setOrigin(tailleTourelle/2,tailleTourelle/2);
    degats=DEGATS_DEFAUT;
    portee=PORTEE_DEFAUT;
    tpsRechargement=RECHARGE_DEFAUT;
}

void Tourelle::changementType(int bonus)//!\ quand clic -> menu avec 3 btn correspondant aux amelioration avec ID appell� "bonus"
{
    switch(bonus)
    {
    case 1://rechargement
        if(tpsRechargement<REDUC_RECHARGE)
        {
            tpsRechargement-=REDUC_RECHARGE;
        }


    case 2://degats
        if(degats+BONUS_DEGATS<MAX_VIE_ENNEMI)
        {
            degats+=BONUS_DEGATS;
        }

    case 3://portee
        if(portee+BONUS_PORTEE<MAX_PORTEE)
        {
            portee+=BONUS_PORTEE;
        }

    }
}

void Tourelle::drawTourelle(RenderWindow& fen)
{
    if (visible==true)
    {
        fen.draw(tourelle);
        Tourelle::rechargement();
    }
}

Tourelle::~Tourelle()
{

}






