#include <SFML/Graphics.hpp>
#include <windows.h>
#include <SFML/Audio.hpp>

#define NOM_FOND "lib/fond.jpg"
#define NOM_CROIX "lib/trait.png"

#define TAILLE_TITRE 150
#define TAILLE_CARACTERES 50
#define POLICE "lib/ASMAN.ttf"

#define DEMI_LARGEUR_TITRE (titre.getLocalBounds().width)/2
#define DEMI_LARGEUR_JOUER (jouer.getLocalBounds().width)/2
#define DEMI_LARGEUR_REGLES (regles.getLocalBounds().width)/2
#define DEMI_LARGEUR_QUITTER (quitter.getLocalBounds().width)/2

#define RATIO_Y_TITRE 1.0/8.0
#define RATIO_Y_JOUER 4.0/8.0
#define RATIO_Y_REGLES 5.0/8.0
#define RATIO_Y_QUITTER 6.0/8.0

#define HAUTEUR_JOUER (jouer.getLocalBounds().height)
#define HAUTEUR_REGLES (regles.getLocalBounds().height)
#define HAUTEUR_QUITTER (quitter.getLocalBounds().height)

#define ZONE_JOUER (( event.mouseButton.x>fenx/2.0-dimensionsTextes[0][0] && event.mouseButton.x<fenx/2.0+dimensionsTextes[0][0] ) && ( event.mouseButton.y > feny*RATIO_Y_JOUER && event.mouseButton.y <= feny*RATIO_Y_JOUER+dimensionsTextes[0][1] ))
#define ZONE_REGLES (( event.mouseButton.x>fenx/2.0-dimensionsTextes[1][0] && event.mouseButton.x<fenx/2.0+dimensionsTextes[1][0] ) && ( event.mouseButton.y > feny*RATIO_Y_REGLES && event.mouseButton.y <= feny*RATIO_Y_REGLES+dimensionsTextes[1][1] ))
#define ZONE_QUITTER (( event.mouseButton.x>fenx/2.0-dimensionsTextes[2][0] && event.mouseButton.x<fenx/2.0+dimensionsTextes[2][0] ) && ( event.mouseButton.y > feny*RATIO_Y_QUITTER && event.mouseButton.y <= feny*RATIO_Y_QUITTER+dimensionsTextes[2][1] ))

#define SUR_JOUER (( event.mouseMove.x>fenx/2-dimensionsTextes[0][0] && event.mouseMove.x<fenx/2.0+dimensionsTextes[0][0] ) && ( event.mouseMove.y > feny*RATIO_Y_JOUER && event.mouseMove.y < feny*RATIO_Y_JOUER+dimensionsTextes[0][1] ))
#define SUR_REGLES (( event.mouseMove.x>fenx/2-dimensionsTextes[1][0] && event.mouseMove.x<fenx/2.0+dimensionsTextes[1][0] ) && ( event.mouseMove.y > feny*RATIO_Y_REGLES && event.mouseMove.y < feny*RATIO_Y_REGLES+dimensionsTextes[1][1] ))
#define SUR_QUITTER ((event.mouseMove.x>fenx/2-dimensionsTextes[2][0] && event.mouseMove.x<fenx/2.0+dimensionsTextes[2][0] ) && ( event.mouseMove.y > feny*RATIO_Y_QUITTER && event.mouseMove.y < feny*RATIO_Y_QUITTER+dimensionsTextes[2][1] ))

#define FOND_L 1920
#define FOND_H 1080


using namespace sf;



void afficheRegles(RenderWindow *pfen, int dimx, int dimy)
{
    // choix de la police � utiliser
    Font font;
    if (!font.loadFromFile(POLICE))
        printf("pb police");

//Creation regles//

    Text rules;

    rules.setFont(font);
    // choix de la phrase
    rules.setString("Regles:\n\t-Survivre aux vagues\n\t-Gagnez de l'argent\n\t-Achetez des batiments et\n\ttourelles par le menu en bas du jeu");
    // choix de la taille des caract�res
    rules.setCharacterSize(TAILLE_CARACTERES);
    // choix du style du texte
    rules.setStyle(Text::Bold | Text::Italic);

    rules.setPosition(0,dimy*5/8);

    rules.setColor(Color(255,223,0));

    pfen -> draw(rules);
}

void afficheCredit(RenderWindow *pfen, int dimx, int dimy)
{
    // choix de la police � utiliser
    Font font;
    if (!font.loadFromFile(POLICE))
        printf("pb police");

//Creation credit//

    Text credit;

    credit.setFont(font);
    // choix de la phrase
    credit.setString("CREDIT:\nMEDARD Mathis\nPACCOUD William\nWADOUX Nicolas");
    // choix de la taille des caract�res
    credit.setCharacterSize(TAILLE_CARACTERES);
    // choix du style du texte
    credit.setStyle(Text::Bold | Text::Italic);

    credit.setPosition(0,dimy*5/8);

    credit.setColor(Color(255,223,0));

    pfen -> draw(credit);
}
void drawFond(RenderWindow *pfen, int dimx, int dimy)
{
    Texture imgFond ;
    if (!imgFond.loadFromFile(NOM_FOND))
        printf("PB de chargement de l'image \n");
    Sprite fond;
    fond.setTexture(imgFond);
    int PosSpriteX = dimx / 2 - FOND_L / 2 ;
    int PosSPriteY = dimy / 2 - FOND_H / 2 ;
    fond.setPosition(PosSpriteX,PosSPriteY);
    pfen -> draw(fond);
}

void drawAccueil(RenderWindow *pfen, int dimx, int dimy, float pdimensionsTextes[10][10])
{
// choix de la police � utiliser
    Font font;
    if (!font.loadFromFile(POLICE))
        printf("pb police");

//Creation titre//

    Text titre;

    titre.setFont(font);
    // choix de la phrase
    titre.setString("Xtrem Defense");
    // choix de la taille des caract�res
    titre.setCharacterSize(TAILLE_TITRE);
    // choix du style du texte
    titre.setStyle(Text::Bold | Text::Underlined);

    titre.setPosition(dimx/2-DEMI_LARGEUR_TITRE,dimy*RATIO_Y_TITRE);

    titre.setColor(Color(115,8,0));

    pfen -> draw(titre);

//Creation texte "jouer"//

    Text jouer;

    jouer.setFont(font);
    // choix de la phrase
    jouer.setString("Jouer");
    // choix de la taille des caract�res
    jouer.setCharacterSize(TAILLE_CARACTERES);
    // choix du style du texte
    jouer.setStyle(Text::Bold);

    jouer.setPosition(dimx/2-DEMI_LARGEUR_JOUER,dimy*RATIO_Y_JOUER);

    pfen -> draw(jouer);

    pdimensionsTextes[0][0]=DEMI_LARGEUR_JOUER;
    pdimensionsTextes[0][1]=HAUTEUR_JOUER;


//Creation texte "regles"//

    Text regles;

    regles.setFont(font);
    // choix de la phrase
    regles.setString("Regles");
    // choix de la taille des caract�res
    regles.setCharacterSize(TAILLE_CARACTERES);
    // choix du style du texte
    regles.setStyle(Text::Bold);

    regles.setPosition(dimx/2-DEMI_LARGEUR_REGLES,dimy*RATIO_Y_REGLES);

    pfen -> draw(regles);

    pdimensionsTextes[1][0]=DEMI_LARGEUR_REGLES;
    pdimensionsTextes[1][1]=HAUTEUR_REGLES;

//Creation texte "quitter"//

    Text quitter;

    quitter.setFont(font);
    // choix de la phrase
    quitter.setString("Quitter");
    // choix de la taille des caract�res
    quitter.setCharacterSize(TAILLE_CARACTERES);
    // choix du style du texte
    quitter.setStyle(Text::Bold);

    quitter.setPosition(dimx/2-DEMI_LARGEUR_QUITTER,dimy*RATIO_Y_QUITTER);

    pfen -> draw(quitter);

    pdimensionsTextes[2][0]=DEMI_LARGEUR_QUITTER;
    pdimensionsTextes[2][1]=HAUTEUR_QUITTER;
}

void creerTraitSelect(RenderWindow *pfen, int indice,int longeur, int hauteur,int dimx,int dimy)
{
    Texture cross;
    if (!cross.loadFromFile(NOM_CROIX))
        printf("PB de chargement de l'image de la croix de selection \n");

    RectangleShape selection(Vector2f(longeur*2,hauteur));

    if (indice==0)//play
    {
        selection.setPosition(dimx/2-longeur,dimy*RATIO_Y_JOUER);
    }
    else if (indice==1)//Regles
    {
        selection.setPosition(dimx/2-longeur,dimy*RATIO_Y_REGLES);
    }
    else if (indice==2)//Quitter
    {
        selection.setPosition(dimx/2-longeur,dimy*RATIO_Y_QUITTER);
    }

    selection.setTexture(&cross);
    pfen -> draw(selection);

}

int main()
{
    float dimensionsTextes [10][10];// permet de recup les dimensions des textes pour les zones de clique 1-jouer 2-regles 3-quit // 0-x 1-y

    int fenx = GetSystemMetrics(SM_CXSCREEN);
    int feny = GetSystemMetrics(SM_CYSCREEN);//permet le full screen
    RenderWindow fen(VideoMode(fenx,feny), "SFML window",Style::Fullscreen);

    fen.setFramerateLimit(60);
    int jouer=0;
    bool Credit=false;
    int regles=1;

    // Loop accueil
    while (fen.isOpen() && jouer==0)
    {
        fen.clear();
        drawFond(&fen, fenx, feny);
        drawAccueil(&fen, fenx, feny,dimensionsTextes);

        Event event;

        while (fen.pollEvent(event))
        {
            if (event.type == Event::MouseButtonPressed)
            {


                if(event.mouseButton.button == Mouse::Right)//Afficher les credits
                {
                    Credit=true;
                }
                else if(ZONE_QUITTER)
                {
                    fen.close();
                }
                else if (ZONE_JOUER)
                {
                    jouer=1;
                    break;
                }
                else if (ZONE_REGLES)
                {
                    regles++;
                }
            }
            if (event.type == Event::MouseButtonReleased)
            {
                Credit=false;
            }

        }


        if (Credit)
        {
            afficheCredit(&fen,fenx,feny);
        }

        if(regles%2 == 0)
        {
            afficheRegles(&fen,fenx,feny);
        }

        if (SUR_QUITTER)
        {

            creerTraitSelect(&fen,2,dimensionsTextes[2][0],dimensionsTextes[2][1],fenx,feny);//indice de deux veut dire "quitter"
        }
        else if (SUR_REGLES)
        {
            creerTraitSelect(&fen,1,dimensionsTextes[1][0],dimensionsTextes[1][1],fenx,feny);
        }
        else if (SUR_JOUER)
        {
            creerTraitSelect(&fen,0,dimensionsTextes[0][0],dimensionsTextes[0][1],fenx,feny);
        }
        fen.display();
    }
    return EXIT_SUCCESS;
}
