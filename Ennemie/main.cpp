#include <SFML/Graphics.hpp>
#include "personnage.hpp"
#include <ctime>
#define GUER "lib/guerrier.png"
#define ARCH "lib/archer.png"
#define MAGE "lib/mage.png"
#define TROL "lib/troll.png"
#define CAVA "lib/cavalier.png"
#define GOBE "lib/gobelin.png"
#define SQUE "lib/squelette.png"
#define DRAG "lib/dragon.png"
#define VAMP "lib/vampire.png"
#define GOLE "lib/golem.png"
#define NBMONSTRE 100
#define NBTYPEMONSTRE 10
#define TEMPSVAGUE 1

using namespace sf;


bool tousMort(Personnage ennemis[ ],int longueur)
{
    int i;
    int compte=0;
    bool resultat;
    for(i=0; i<longueur; i++)
    {
        if(ennemis[i].visible==false)
            compte++;
    }
    if(compte>=longueur)
        resultat=true;
    else
        resultat=false;
    return resultat;
}

int main()
{
    //Apparition d'une fenetre

    srand(time(NULL));
    Event event;
    Clock clock;
    int goldTot=0;
    int totMonstre=NBMONSTRE*NBTYPEMONSTRE;
    float time;
    int ennemis[10]= {0};
    int i,j,k;
    int numVague=0;
    bool vagueActivation=false;
    bool timerRelance=true;
    int fenx = VideoMode::getDesktopMode().width;
    int feny = VideoMode::getDesktopMode().height;
    RenderWindow fen(VideoMode(fenx, feny), "GAME XD", Style::Fullscreen);
    fen.setFramerateLimit(30);
    time = clock.getElapsedTime().asSeconds();

    //Chargement d'une texture
    Texture guerrierT;
    if(!guerrierT.loadFromFile(GUER))
        printf("ERROR");
    Texture archerT;
    if(!archerT.loadFromFile(ARCH))
        printf("ERROR");
    Texture mageT;
    if(!mageT.loadFromFile(MAGE))
        printf("ERROR");
    Texture trollT;
    if(!trollT.loadFromFile(TROL))
        printf("ERROR");
    Texture cavalierT;
    if(!cavalierT.loadFromFile(CAVA))
        printf("ERROR");
    Texture gobelinT;
    if(!gobelinT.loadFromFile(GOBE))
        printf("ERROR");
    Texture squeletteT;
    if(!squeletteT.loadFromFile(SQUE))
        printf("ERROR");
    Texture dragonT;
    if(!dragonT.loadFromFile(DRAG))
        printf("ERROR");
    Texture vampireT;
    if(!vampireT.loadFromFile(VAMP))
        printf("ERROR");
    Texture golemT;
    if(!golemT.loadFromFile(GOLE))
        printf("ERROR");
    Font font;
    if(!font.loadFromFile(TEXT))
        printf("ERROR");

    //On d�finis un nombre de personnages que l'on va utiliser plus tard
    Personnage monstre[NBMONSTRE*NBTYPEMONSTRE];
    //for(i=0;i<NBMONSTRE*NBTYPEMONSTRE+1;i++){
    for(j=0; j<NBTYPEMONSTRE; j++)
    {
        for(k=0; k<NBMONSTRE; k++)
        {
            Personnage personnage(j,fen,font);
                    switch(j)
                    {
                    case 0:
                        personnage.setTexture(guerrierT);
                        break;
                    case 1:
                        personnage.setTexture(archerT);
                        break;
                    case 2:
                        personnage.setTexture(mageT);
                        break;
                    case 3:
                        personnage.setTexture(trollT);
                        break;
                    case 4:
                        personnage.setTexture(cavalierT);
                        break;
                    case 5:
                        personnage.setTexture(gobelinT);
                        break;
                    case 6:
                        personnage.setTexture(squeletteT);
                        break;
                    case 7:
                        personnage.setTexture(dragonT);
                        break;
                    case 8:
                        personnage.setTexture(vampireT);
                        break;
                    case 9:
                        personnage.setTexture(golemT);
                        break;
                    }
            monstre[j*100+k]=personnage;

        }
    }

    Personnage test(9,fen,font);
    test.setTexture(golemT);

    test.forcePopPerso();
    test.changementCamps();


    //On d�marre le jeu
    while (fen.isOpen())
    {
        Event event;
        while (fen.pollEvent(event))
        {
            switch (event.type)
            {
            case Event::Closed:
                fen.close();
                break;
            case Event::MouseButtonPressed:
                if(event.mouseButton.button==Mouse::Left)
                {
                    for(i=0; i<NBMONSTRE*NBTYPEMONSTRE; i++)
                    {
                        if(monstre[i].visible==true)
                            monstre[i].recevoirDegats(10);
                    }
                }
                break;

            }

        }
        fen.clear(Color(35, 84, 33));

        //Pour qu'un personnage subisse des d�gats
        //attaquant.attaquer(cible)
        if(vagueActivation==true)
        {
            numVague++;
            ennemis[0]=10*numVague;
            ennemis[1]=5*numVague;
            ennemis[2]=4*(numVague/3);
            ennemis[3]=4*(numVague/4);
            ennemis[4]=4*(numVague/5);
            ennemis[5]=10*(numVague/2);
            ennemis[6]=10*(numVague/6);
            ennemis[7]=2*(numVague/8);
            ennemis[8]=5*(numVague/7);
            ennemis[9]=(numVague/10);
            for(k=0; k<NBTYPEMONSTRE; k++)
            {
                for(i=0; i<ennemis[k]; i++)
                {
                    monstre[i+NBMONSTRE*k].popPerso();
                }
            }

            timerRelance=false;
            vagueActivation=false;

        }
        else if(tousMort(monstre,totMonstre)==true && timerRelance==true)
        {
            time = clock.getElapsedTime().asSeconds();
            if(time>=TEMPSVAGUE)
                vagueActivation=true;
        }
        else if(tousMort(monstre,totMonstre)==true && timerRelance==false)
        {
            clock.restart();
            timerRelance=true;
        }


        //On update les perso voulu pour les faire apparaitre
        for(i=0; i<NBMONSTRE*NBTYPEMONSTRE; i++)
        {
            if(monstre[i].mort==true)
            {
                goldTot=goldTot+monstre[i].goldWin;
                monstre[i].mort=false;
                printf("Augmentation de gold : %i\n",goldTot);
            }
            monstre[i].update(fen,test);
            test.update(fen,test);
        }
        fen.display();
    }

    return 0;
}

