#ifndef DEF_PERSONNAGE
#define DEF_PERSONNAGE
#define TEXT "lib/ASMAN.ttf"
#include <string>
#include <SFML/Graphics.hpp>
#include <ctime>

class Personnage
{
public:

    Personnage();
    Personnage(int typePerso, sf::RenderWindow& fen,sf::Font& font);
    ~Personnage();
    void recevoirDegats(int nbDegats);
    void attaquer(Personnage &cible);
    void rechargement();
    void estVivant();
    void draw(sf::RenderWindow& fen);
    void setTexture(sf::Texture& persoTexture);
    void up(float modif);
    void down(float modif);
    void left(float modif);
    void right(float modif);
    void popPerso();
    void forcePopPerso();
    void update(sf::RenderWindow& fen,Personnage &cibletest);
    void changementCamps();
    void setScoreMort(sf::Font& font);
    void drawScoreMort(sf::RenderWindow& fen);
    void compteurScore();
    bool visible;
    float posX;
    float posY;
    int vie;
    int goldWin;
    bool mort;

private:

    int typePerso;
    int vieMax;
    float vitesse;
    int portee;
    int degats;
    int animeAttaque;
    int camps;
    int recharge;
    int tempsRecharge;
    int gold;
    sf::Sprite personnage;
    sf::Sprite animation;
    sf::RectangleShape barVie;
    sf::RectangleShape barVieFond;
    sf::Text goldText;
    sf::Font font;
    float taille;
    float posDepX;
    float posDepY;
    int tempsApresMort;
};

#endif
