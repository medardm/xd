#include <SFML/Graphics.hpp>
#include <ctime>
#include "personnage.hpp"
#include <string>
#define TEXT "lib/ASMAN.ttf"
#define FRAMERATE 30
#include <math.h>

using namespace sf;
Personnage::Personnage()
{

}

void Personnage::recevoirDegats(int nbDegats)
{
    if(visible==true)
    {
    vie=vie-nbDegats;
    if(vie<=0)
    {
        vie=0;
    }
    //printf("%i degat recus, vie: %i\n ",nbDegats,vie);
    Personnage::estVivant();
    }
}
void Personnage::attaquer(Personnage &cible)
{
    Personnage::rechargement();
    //ciblage de la personne
    float xCible,yCible,xPerso,yPerso,x,y,xfen,yfen,signeX,signeY,pourcentX,pourcentY,distanceX,distanceY;
    xCible=cible.posX;
    yCible=cible.posY;
    xPerso=posX;
    yPerso=posY;
    xfen=(VideoMode::getDesktopMode().width)/2;
    yfen=(VideoMode::getDesktopMode().height)/2;
    signeX = 1;
    signeY = 1;
    distanceX=(xCible)-xPerso;
    distanceY=(yCible)-yPerso;

    if(xPerso>xCible)
        {
        signeX=-1;
        distanceX=xPerso-(xCible);
        }
    if(yPerso>yCible)
        {
        signeY=-1;
        distanceY=yPerso-(yCible);
        }

    pourcentX = distanceX / (distanceX + distanceY);
    pourcentY = distanceY / (distanceX + distanceY);
    x=pourcentX * vitesse*2;
    y=pourcentY * vitesse*2;

    if(cible.camps!=camps && cible.visible==true && (distanceX>=portee+cible.taille/2 || distanceY>=portee+cible.taille/2))
    {
    posX=posX+x*signeX;
    posY=posY+y*signeY;
    }


    //Recevoir d�gats
    if(posX<xfen && cible.visible==true && recharge==tempsRecharge)
    {
        if(posY<yfen)
        {

            if (xPerso+portee>=xCible-cible.taille/2 && yPerso+portee>=yCible-cible.taille/2)
            {
                cible.recevoirDegats(degats);
                recharge=0;
            }
        }
        else if(posY>yfen)
        {
            if (xPerso+portee>=xCible-cible.taille/2 && yPerso-portee<=yCible+cible.taille/2)
            {
                cible.recevoirDegats(degats);
                recharge=0;
            }
        }
    }
    else if(posX>xfen && cible.visible==true && recharge==tempsRecharge)
    {
        if(posY<yfen)
        {
            if (xPerso-portee<=xCible+cible.taille/2 && yPerso+portee>=yCible-cible.taille/2)
            {
                cible.recevoirDegats(degats);
                recharge=0;
            }
        }
        else if(posY>yfen)
        {

            if (xPerso-portee<=xCible+cible.taille/2 && yPerso-portee<=yCible+cible.taille/2)
            {
                cible.recevoirDegats(degats);
                recharge=0;
            }
        }
    }
}
void Personnage::rechargement()
{
    recharge++;
    if(recharge>=tempsRecharge)
        recharge=tempsRecharge;
}

void Personnage::estVivant()
{
    if(vie<=0)
    {
        visible=false;
        mort=true;
        goldWin=gold;

    }
    else
    {
        visible=true;

    }
}
void Personnage::setScoreMort(Font& font)
{
    char pGold[10];
    sprintf(pGold,"%i",gold);
    goldText.setFont(font);
    goldText.setString(pGold);
    goldText.setOrigin((goldText.getLocalBounds().width)/2,0);
    goldText.setColor(Color::White);
}


Personnage::Personnage(int typePerso, RenderWindow& fen,Font& font)
{
    switch(typePerso)
    {
    case 0: //guerrier
        vie=100; //normal
        degats = 10; //normal
        vitesse = 0.5*2; //normal
        portee = 10; //corp � corp
        taille=50;//normal
        tempsRecharge=FRAMERATE*2;//normal
        gold=10;//
        animeAttaque=1;//�p�e
        break;
    case 1: //archer
        vie=60; //reduit
        degats = 10; //normal
        vitesse = 0.5*2; //normal
        portee = 100; //distance
        taille=50;//normal
        tempsRecharge=FRAMERATE*2;//normal
        gold=20;//
        animeAttaque=2;//arc
        break;
    case 2: //Mage
        vie=50; //reduit
        degats = 20; //normal
        vitesse = 0.5*2; //normal
        portee = 100; //distance
        taille=50;//normal
        tempsRecharge=FRAMERATE*3;//lent
        gold=50;//
        animeAttaque=3;//boule de feu
        break;
    case 3: //Troll
        vie=120; //augment�
        degats = 20; //normal
        vitesse = 0.2*2; //lent
        portee = 10; //corp � corp
        taille=70;//�pais
        tempsRecharge=FRAMERATE*3;//lent
        gold=80;//
        animeAttaque=1;//�p�e
        break;
    case 4: //Cavalier
        vie=100; //normal
        degats = 15; //augmnt�
        vitesse = 1*2; //rapide
        portee = 10; //c�c
        taille=50;//�pais
        tempsRecharge=FRAMERATE*2;//normal
        gold=100;//
        animeAttaque=1;//�p�e
        break;
    case 5: //gobelin
        vie=20; //20
        degats = 5; //normal
        vitesse = 1.3*2; //tres rapide
        portee = 10; //corp � corp
        taille=25;//petit
        tempsRecharge=FRAMERATE*2;//rapide
        gold=10;//
        animeAttaque=1;//�p�e
        break;
    case 6: //squelette
        vie=80; //normal
        degats = 5; //normal
        vitesse = 0.5*2; //normal
        portee = 10; //corp � corp//normal
        taille=25;//petit
        tempsRecharge=FRAMERATE*1;//rapide
        gold=20;//
        animeAttaque=1;//�p�e
        break;
    case 7: //Dragon
        vie=200; //�lev�
        degats = 20; //normal
        vitesse = 0.2*2; //lent
        portee = 80; //petite distance
        taille=100;//Grand
        tempsRecharge=FRAMERATE*2;//normal
        gold=150;//
        animeAttaque=3;//boule de feu
        break;
    case 8: //Vampire
        vie=80; //diminu�
        degats = 15; //augment�
        vitesse = 0.5*2; //normal
        portee = 10; //corp � corp
        taille=50;//Normal
        tempsRecharge=FRAMERATE*2;//normal
        gold=50;//
        animeAttaque=4;//a la mano
        break;
    case 9: //Golem
        vie=500; //Tr�s �lev�
        degats = 50; //normal
        vitesse = 0.1*2; //tr�s lent
        portee = 10; //corp � corp
        taille=100;//Grand
        tempsRecharge=FRAMERATE*5;//Tres lent
        gold=200;//
        animeAttaque=4;//a la mano
        break;
    }
    visible=false;
    camps=1;
    vieMax=vie;
    Personnage::setScoreMort(font);
    mort=false;

    barVie.setFillColor(Color::Green);
    barVieFond.setFillColor(Color::Red);
    barVieFond.setSize(Vector2f(vieMax,10));
    barVie.setSize(Vector2f(vie,barVieFond.getSize().y));
    barVieFond.setOrigin(Vector2f(vieMax/2,5));
}
void Personnage::setTexture(sf::Texture& persoTexture)
{
    personnage.setTexture(persoTexture);
    personnage.setOrigin(Vector2f(persoTexture.getSize().x/2,persoTexture.getSize().y/2));
    personnage.setScale(taille/250,taille/250);
}

void Personnage::update(RenderWindow& fen,Personnage &cibletest)
{
    if(visible==true)
    {
        personnage.setPosition(posX,posY);
        Personnage::attaquer(cibletest);

        barVie.setPosition(barVieFond.getPosition().x-vieMax/2,barVieFond.getPosition().y-5);
        barVieFond.setPosition(posX,posY-taille);
        Personnage::barVie.setSize(Vector2f(vie*barVieFond.getSize().x/vieMax,barVieFond.getSize().y));


        Personnage::draw(fen);
    }
    else if(vie<=0 && tempsApresMort<FRAMERATE)
    {
        tempsApresMort++;
        if(tempsApresMort<FRAMERATE)
        {
            drawScoreMort(fen);
        }

    }
}

void Personnage::popPerso(/*Batiment murs*/)
{
    if(visible==false)
    {

        int coteApparition=rand()%4+1;
        switch (coteApparition)
        {
        case 1: //haut
            posY=-100-rand()%200;
            posX=rand()%(VideoMode::getDesktopMode().width);
            //attaque(Batiment murs[0])
            break;
        case 2: //droite
            posY=rand()%(VideoMode::getDesktopMode().height);
            posX=(VideoMode::getDesktopMode().width)+100+rand()%200;
            //attaque(Batiment murs[1]
            break;
        case 3: //bas
            posY=(VideoMode::getDesktopMode().height)+100+rand()%200;
            posX=rand()%(VideoMode::getDesktopMode().width);
            //attaque(Batiment murs[2]
            break;
        case 4: //gauche
            posY=rand()%(VideoMode::getDesktopMode().height);
            posX=-100-rand()%200;
            //attaque(Batiment murs[3]
            break;
        }
        visible=true;
        posDepX=posX;
        posDepY=posY;
        vie=vieMax;
        tempsApresMort=0;
        goldWin=0;
    }
}
void Personnage::forcePopPerso()
{

    if(visible==false)
    {
        posY=(VideoMode::getDesktopMode().height)/2;
        posX=(VideoMode::getDesktopMode().width)/2;
        visible=true;
        posDepX=posX;
        posDepY=posY;
        tempsApresMort=0;
        goldWin=0;
    }

}

void Personnage::changementCamps()
{
    if(camps==1)
    {
        camps=0;
        personnage.setColor(Color(42,159,201));
    }
    else if(camps==0)
    {
        camps=1;
        personnage.setColor(Color::Red);
    }
}

void Personnage::draw(RenderWindow& fen)
{
    fen.draw(personnage);
    if(vie<vieMax)
    {
        fen.draw(barVieFond);
        fen.draw(barVie);
    }
}
void Personnage::drawScoreMort(RenderWindow& fen)
{
        goldText.setPosition(Vector2f(posX,posY-taille));
        fen.draw(goldText);
}


Personnage::~Personnage()
{

}

